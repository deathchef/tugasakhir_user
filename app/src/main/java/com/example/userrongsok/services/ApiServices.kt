package com.example.userrongsok.services

import com.example.userrongsok.model.BodyPayloadNotif
import com.example.userrongsok.util.ConstantsApp
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiServices {
    @POST(ConstantsApp.SEND_NOTIF)
    @Headers("Content-Type: ${ConstantsApp.CONTENT_TYPE}")
    fun pushNotif(
        @Header("Authorization") token: String,
        @Body bodyPayLoadNotif: BodyPayloadNotif
    ): Single<String>
}