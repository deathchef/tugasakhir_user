package com.example.userrongsok.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_HIGH
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_ONE_SHOT
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.example.userrongsok.R
import com.example.userrongsok.model.BodyPayloadNotif
import com.example.userrongsok.ui.mainmenu.transaksi.TransaksiFragment
import com.example.userrongsok.ui.notification.NotificationActivity
import com.example.userrongsok.util.ConstantsApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import io.karn.notify.Notify
import org.json.JSONObject
import java.util.*
import kotlin.random.Random

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private val TAG = MyFirebaseMessagingService::class.java.simpleName

    lateinit var manager: NotificationManager
    var auth = FirebaseAuth.getInstance()
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        var id = auth.currentUser?.uid
        Log.d(TAG, "From: ${remoteMessage.from}")


        remoteMessage.data.isNotEmpty().let {
            Log.d("Message Data Payload: ", remoteMessage.data.toString())
            val jsonData = Gson().toJson(remoteMessage.data)
            Log.d(TAG, "Response: $jsonData")

            sendBroadcast(Intent(this, BroadcastReceiver::class.java))

            if (jsonData != null) {
                val jsonObject = JSONObject(jsonData)
                val type = jsonObject.getString("type")
                when (type) {
                    ConstantsApp.SHOP -> {
                        val model = Gson().fromJson(jsonData, BodyPayloadNotif.Data::class.java)
                        if (id == model.id_userRongsok) {
                            showNotification(model.title.toString(), model.message.toString())
                        }
                    }
                }
            }
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            manager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            showNotification(
                messageTitle = it.title.toString(),
                messageBody = it.body.toString()
//                typeData = typeData
            )
            Log.d(TAG, "Message Notification Body: ${it.body}")
        }

    }

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
        sendRegistrationToServer(token)
    }

    private fun scheduleJob() {
        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        WorkManager.getInstance().beginWith(work).enqueue()
    }

    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
        Log.d(TAG, "sendRegistrationTokenToServer($token)")
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun showNotification(
        messageTitle: String,
        messageBody: String,
//        typeData: String
    ) {

        val CHANNEL_ID = getString(R.string.default_notification_channel_id)
        val notificationID = R.integer.id_notification

        val intent = Intent(applicationContext, NotificationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var channel = NotificationChannel(
                CHANNEL_ID,
                "pushNotification",
                NotificationManager.IMPORTANCE_HIGH
            )
            manager.createNotificationChannel(channel)
        }

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(messageTitle)
            .setContentText(messageBody)
            .setSmallIcon(R.drawable.ic_baseline_notifications_24)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)

        var pendingIntent =
            PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        builder.setContentIntent(pendingIntent)
        manager.notify(0, builder.build())
    }
}