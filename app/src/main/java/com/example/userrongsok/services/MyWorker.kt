package com.example.userrongsok.services

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters

class MyWorker(appContext: Context, workerParams: WorkerParameters): Worker(appContext, workerParams) {
    override fun doWork(): Result {
        Log.d("My Worker", "Performing Long Running Task In Schedule Job")
        return ListenableWorker.Result.success()
    }
}