package com.example.userrongsok.ui.mainmenu.historytransaksi

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Transaksi
import com.example.userrongsok.repo.firebase.FirebaseTransaksiRepo
import com.example.userrongsok.util.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class HistoryViewModel(
    private val auth: FirebaseAuth,
    private val repoTransaksi: FirebaseTransaksiRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {

    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _dataHistoryTransaksi = MutableLiveData<MutableList<Transaksi>>()
    val dataHistoryTransaksi: LiveData<MutableList<Transaksi>> get() = _dataHistoryTransaksi

    fun getDataHistory(){
        CoroutineScope(dispatchers.io()).launch {
            try {
                when(val result = repoTransaksi.getAllTransaksi(auth.currentUser?.uid.toString())){
                    is RepositoryResult.Success->{
                        _dataHistoryTransaksi.postValue(result.data)
                        Log.d("History Data","result: ${result.data}")
                    }
                    is RepositoryResult.Error->{
                        _errorLiveData.postValue("Error: Gagal Memuat Data History")
                        Log.d("Error Data","Gagal memuat data history transaksi")
                    }
                    is RepositoryResult.Canceled->{
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                            Log.d("Cancel Data","Memuat Data Dibatalkan")
                        }
                    }
                }

            }catch (e:Exception){
                withContext(dispatchers.main()){
                    e.message?.let {
                        Log.d("Exception MSG", "Exception Data Transaksi")
                    }
                }
            }
        }
    }
    class Factory(
        private val auth: FirebaseAuth,
        private val repoTransaksi: FirebaseTransaksiRepo,
        private val dispatchers: DispatcherProvider
    ): ViewModelProvider.NewInstanceFactory(){
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return HistoryViewModel(auth, repoTransaksi, dispatchers) as T
        }
    }
}