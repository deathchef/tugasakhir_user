package com.example.userrongsok.ui.mainmenu.tentang

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.userrongsok.R
import kotlinx.android.synthetic.main.activity_tentang.*

class Tentang : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tentang)

        body_text.setText(
            "  Sebuah aplikasi pencari dan pemanggil pengepul sekitar untuk memudahkan masyarakat dalam menukarkan barang bekas yang dimiliki.\n\n"
                    + "  Pengepul dan pelanggan bertemu dalam satu platform yang dapat saling menemukan, berbagi informasi, dan berinteraksi untuk kebutuhan transaksi. \n\n"
                    + "  Aplikasi ini dibuat dengan harapan mampu mengurangi jumlah sampah yang akan mendatang, mempermudah transaksi jual beli rongsok atau barang bekas, serta mendatangkan keuntungan bagi penggunanya."
        )
    }
}