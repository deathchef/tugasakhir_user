package com.example.userrongsok.ui.mainmenu

import android.Manifest
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.userrongsok.R
import com.example.userrongsok.ui.mainmenu.historytransaksi.HistoryTransaksi
import com.example.userrongsok.ui.notification.NotificationActivity
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.ktx.messaging
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.layout_main.*
import kotlinx.android.synthetic.main.main.*

class MainMenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)

        FirebaseMessaging.getInstance().isAutoInitEnabled = true

        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(null)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setHomeButtonEnabled(false)
        val navbarConf = findNavController(R.id.nav_host_fragment)
        button_nav.setupWithNavController(navbarConf)

        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.INTERNET,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    Log.d("PERMISSION", "CHECK")
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {
                    Log.d("PERMISSION","RATIONAL")
                }
            }).check()

        navbarConf.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.fragment_pengepul -> {
                    btn_notification.setImageResource(0)
                    tv_label.setText("Pengepul")
                    btn_image.setImageResource(0)
                }
                R.id.fragment_transaksi -> {
                    btn_notification.setImageResource(R.drawable.ic_baseline_notifications_24)
                    btn_notification.setOnClickListener { startActivity(Intent(this, NotificationActivity::class.java)) }
                    tv_label.setText("Transaksi")
                    btn_image.setImageResource(R.drawable.ic_baseline_history_24)
                    btn_image.setOnClickListener { startActivity(Intent(this,HistoryTransaksi::class.java)) }
                }
                R.id.fragment_inbox -> {
                    btn_notification.setImageResource(0)
                    tv_label.setText("Inbox")
                    btn_image.setImageResource(0)
                }
                R.id.fragment_profil -> {
                    btn_notification.setImageResource(0)
                    tv_label.setText("Profil")
                    btn_image.setImageResource(0)
                }
            }
        }
    }
}