package com.example.userrongsok.ui.mainmenu.pengepul.listpengepul

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.userrongsok.R
import com.example.userrongsok.model.RongsokAdapter
import com.example.userrongsok.model.TokoAdapter
import kotlinx.android.synthetic.main.item_list_pengepul.view.*

class AdapterPengepul():RecyclerView.Adapter<AdapterPengepul.listPengepulHolder>() {
    private val listToko = mutableListOf<TokoAdapter>()
    private lateinit var listener: OnClickListenerCallback

    fun setOnClickListener(listenerCallback: OnClickListenerCallback){
        this.listener = listenerCallback
    }
    inner class listPengepulHolder(private val view: View): RecyclerView.ViewHolder(view) {
        fun bind(toko: TokoAdapter){

            if (toko.rating == 0.0){
                view.tv_rating.text = toko.rating.toString()
            }else{
                var rataRating = toko.rating/toko.pengunjung
                view.tv_rating.text = rataRating.toString().subSequence(0,3)
            }

            view.tv_namaToko.text = toko.nama_toko
            Glide.with(view.context)
                .load(toko.foto_tokoUri)
                .into(view.iv_toko)

            if (toko.isOpen == true){
                view.setBackgroundColor(Color.WHITE)
                view.tv_toko_tutup.visibility = View.INVISIBLE
                listener.let {
                    view.setOnClickListener {
                        listener.onClickListenerCallback(toko)
                    }
                }
            }else{
                view.setBackgroundColor(Color.GRAY)
                view.tv_toko_tutup.visibility = View.VISIBLE
            }
        }
    }
    fun setData(listToko: List<TokoAdapter>){
        this.listToko.clear()
        this.listToko.addAll(listToko)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterPengepul.listPengepulHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_pengepul, parent, false)
        return listPengepulHolder(view)
    }

    override fun onBindViewHolder(holder: AdapterPengepul.listPengepulHolder, position: Int) {
        holder.bind(listToko[position])
    }

    override fun getItemCount(): Int {
        return listToko.size
    }
    interface OnClickListenerCallback{
        fun onClickListenerCallback(tokoAdapter: TokoAdapter)
    }
}