package com.example.userrongsok.ui.mainmenu.detailtransaksi

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.userrongsok.R
import com.example.userrongsok.model.RongsokAdapter
import com.example.userrongsok.model.Transaksi
import kotlinx.android.synthetic.main.item_detail_rongsok.view.*

class AdapterDetailTransaksi(var context: Context, var listRongsokAdapter: MutableList<RongsokAdapter>) :
    RecyclerView.Adapter<AdapterDetailTransaksi.listDetailRongsokHolder>() {
//    private val listRongsok = mutableListOf<RongsokAdapter>()

    inner class listDetailRongsokHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(rongsok: RongsokAdapter) {

            itemView.tv_nama_rongsokDetail.text = rongsok.nama_rongsok
            itemView.tv_beratRongsok.text = rongsok.quantity.toString()
            itemView.tv_harga_rongsok_transaksi.text = rongsok.harga_rongsok.toString()
            Glide.with(view.context)
                .load(rongsok.foto_rongsokUri)
                .into(itemView.iv_fotoDetailRongsok)
        }

    }
    fun setData(listRongsok:List<RongsokAdapter>){
        listRongsokAdapter.clear()
        listRongsokAdapter.addAll(listRongsok)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDetailTransaksi.listDetailRongsokHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_detail_rongsok, parent, false)
        return listDetailRongsokHolder(view)
    }

    override fun onBindViewHolder(
        holder: AdapterDetailTransaksi.listDetailRongsokHolder,
        position: Int
    ) {
        holder.bind(listRongsokAdapter[position])
    }

    override fun getItemCount(): Int {
        return listRongsokAdapter.size
    }
}