package com.example.userrongsok.ui.mainmenu.inbox

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.userrongsok.R
import kotlinx.android.synthetic.main.item_inbox.view.*

class AdapterInbox(private val data: MutableList<ModelInbox> = mutableListOf()) :
    RecyclerView.Adapter<AdapterInbox.listInboxHolder>() {
    inner class listInboxHolder(private val view: View): RecyclerView.ViewHolder(view) {
        fun bind(inbox: ModelInbox){
            view.tv_namaPengirim.text = inbox.judul
            view.tv_tanggalInbox.text = inbox.tanggal.toString()
            view.tv_isiInbox.text = inbox.pesan
        }

    }

    fun setData(data: MutableList<ModelInbox>){
        this.data.clear()
        this.data.addAll(data)
            notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): listInboxHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_inbox, parent, false)
        return listInboxHolder(view)
    }

    override fun onBindViewHolder(holder: listInboxHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }
}