package com.example.userrongsok.ui.mainmenu.detailpengepul

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.userrongsok.R
import com.example.userrongsok.model.RongsokAdapter
import kotlinx.android.synthetic.main.item_rongsok.view.*
import java.util.ArrayList

class AdapterListRongsok(var context: Context, var listRongsok: ArrayList<RongsokAdapter>) :
    RecyclerView.Adapter<AdapterListRongsok.listRongsokHolder>() {

    private lateinit var listener: OnClickListenerCallback

    fun setOnClickListener(listenerCallback: OnClickListenerCallback) {
        this.listener = listenerCallback
    }

    inner class listRongsokHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(rongsok: RongsokAdapter) {
            itemView.tv_namaRongsok.text = rongsok.nama_rongsok
            itemView.tv_hargaRongsok.text = rongsok.harga_rongsok.toString()
            Glide.with(itemView.context)
                .load(rongsok.foto_rongsokUri)
                .into(itemView.iv_rongsok)

            if (rongsok.isReady) {
                view.btn_plus.visibility = View.VISIBLE
                view.btn_minus.visibility = View.VISIBLE
                view.setBackgroundColor(Color.WHITE)
                view.tv_tidak_tersedia.visibility = View.INVISIBLE
            } else {
                view.btn_plus.visibility = View.INVISIBLE
                view.btn_minus.visibility = View.INVISIBLE
                view.setBackgroundColor(Color.GRAY)
                view.tv_tidak_tersedia.visibility = View.VISIBLE
            }
            listener.let {
                view.btn_plus.setOnClickListener {
                    rongsok.quantity = rongsok.quantity + 1
                    view.tv_banyakItem.text = rongsok.quantity.toString()
                    listener.addItem(rongsok)
                }
                view.btn_minus.setOnClickListener {
                    rongsok.quantity = rongsok.quantity - 1
                    view.tv_banyakItem.text = rongsok.quantity.toString()
                    listener.minItem(rongsok)
                }
            }
    }
}

fun setData(listRongsok: List<RongsokAdapter>) {
    this.listRongsok.clear()
    this.listRongsok.addAll(listRongsok)
    notifyDataSetChanged()
}

override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
): AdapterListRongsok.listRongsokHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_rongsok, parent, false)
    return listRongsokHolder(view)
}

override fun onBindViewHolder(holder: AdapterListRongsok.listRongsokHolder, position: Int) {
    holder.bind(listRongsok[position])
}

override fun getItemCount(): Int {
    return listRongsok.size
}

interface OnClickListenerCallback {
    fun addItem(rongsokAdapter: RongsokAdapter)
    fun minItem(rongsok: RongsokAdapter)
}

}