package com.example.userrongsok.ui.mainmenu.detailtransaksi

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.*
import com.example.userrongsok.repo.firebase.FirebaseNotificationRepo
import com.example.userrongsok.repo.firebase.FirebaseTransaksiRepo
import com.example.userrongsok.services.DataManager
import com.example.userrongsok.util.DispatcherProvider
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.util.ArrayList

class DetailTransaksiViewModel(
    private val auth: FirebaseAuth,
    private val repoNotif: FirebaseNotificationRepo,
    private val repoTransaksi: FirebaseTransaksiRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {

    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _successLiveData = MutableLiveData<String>()
    val successLiveData: LiveData<String> get() = _successLiveData

    private val _rongsokTransaksiLiveData = MutableLiveData<ArrayList<RongsokTransaksi>>()
    val rongsokTransaksiLiveData: LiveData<ArrayList<RongsokTransaksi>> get() = _rongsokTransaksiLiveData

    private val _photoLiveData = MutableLiveData<Uri>()
    val photoLiveData: LiveData<Uri> get() = _photoLiveData

    fun downloadPhoto(nama: String) {
        CoroutineScope(dispatchers.io()).launch {
            try {
                val photo =
                    repoTransaksi.downloadFotoTransaksi(auth.currentUser?.uid.toString(), nama)
                _photoLiveData.postValue(photo)
            } catch (e: Exception) {
                _errorLiveData.postValue("Gagal Menambahkan Foto")
            }
        }
    }

    fun sendNotification(
        notification: BodyPayloadNotif
    ) = DataManager.pushNotif(notification).subscribe({

    },{
        Log.d("DetialVM", it.toString())
    })

    fun addNotif(notificationData: BodyPayloadNotif, idPengepul: String) {
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoNotif.addDataNotif(notificationData, idPengepul)) {
                    is RepositoryResult.Success -> {
                        _successLiveData.postValue(result.data)
                    }
                    is RepositoryResult.Error -> {
                        _errorLiveData.postValue("Gagal Menambah Notif")
                    }
                    is RepositoryResult.Canceled -> {
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception) {
                e.message?.let {
                    _errorLiveData.postValue(it)
                }
            }
        }
    }

    fun uploadFotoTransaksi(nama: String, photo_uri: Uri) {
        CoroutineScope(dispatchers.io()).launch {
            try {
                repoTransaksi.addFotoTransaksi(
                    auth.currentUser?.uid.toString(),
                    nama,
                    photo_uri,
                    onResult = { uri ->
                        _photoLiveData.postValue(uri)
                    })
            } catch (e: Exception) {
                _errorLiveData.postValue(e.message)
                Log.d("Upload Foto Transaksi", e.message.toString())
            }
        }
    }

    fun addRongsok(
        foto_rongsok: String,
        nama_rongsok: String,
        id_user: String,
        id_toko: String,
        harga_rongsok: Int,
        quantity: Int
    ) {
        val rongsok = RongsokTransaksi(
            foto_rongsok,
            nama_rongsok,
            id_user,
            id_toko,
            harga_rongsok,
            quantity
        )
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoTransaksi.addRongsokTransaksi(rongsok, id_user)) {
                    is RepositoryResult.Success -> {
                        Log.d("Rongsok Transaksi", "rongsok: ${result.data}")
                        _rongsokTransaksiLiveData.postValue(result.data)
                    }
                    is RepositoryResult.Error -> {
                        Log.d("Error Add Rongsok", "Gagal Memasukkan Rongsok")
                        _errorLiveData.postValue("Rongsok Gagal ditambah")
                    }
                    is RepositoryResult.Canceled -> {
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception) {
                e.message?.let {
                    _errorLiveData.postValue(it)
                }
            }
        }
    }

    fun addTransaksi(
        catatan: String,
        foto_transaksi: String,
        id_pengepul: String,
        id_user: String,
        isDone: Boolean,
        isProccess: Boolean,
        isCancel: Boolean,
        isRated: Boolean,
        status: String,
        jumlah_harga: Int,
        nama_pelanggan: String,
        nama_toko: String,
        latitude: Double,
        longitude: Double,
        address: String,
        noTelpToko: String,
//        list_foto: ArrayList<String> = arrayListOf(),
//        list_rongsok: ArrayList<String> = arrayListOf(),
//        list_harga: ArrayList<Int> = arrayListOf(),
//        list_quantity: ArrayList<Int> = arrayListOf(),
        tanggal_transaksi: Timestamp = Timestamp.now()
    ) {
        val uid = auth.currentUser?.uid!!
        val nama: String = ""
        val transaksi = Transaksi(
            catatan,
            foto_transaksi = "/transaksi$uid$nama",
//            foto_transaksiUri,
            id_pengepul,
            id_user,
            isDone,
            isProccess,
            isCancel,
            isRated,
            status,
            jumlah_harga,
            nama_pelanggan,
            nama_toko,
            latitude,
            longitude,
            address,
            noTelpToko,
//            list_foto,
//            list_rongsok,
//            list_harga,
//            list_quantity,
            tanggal_transaksi
        )
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoTransaksi.addTransaksi(transaksi)) {
                    is RepositoryResult.Success -> {
                        Log.d("TransaksiVM", "result: ${result.data}")
                        _successLiveData.postValue("Pengajuan Transaksi Rongsok Sukses")
                    }
                    is RepositoryResult.Error -> {
                        Log.d("TransaksiVMError", "result:${result.exception}")
                        _errorLiveData.postValue("Gagal Memasukkan Data Transaksi")
                    }
                    is RepositoryResult.Canceled -> {
                        Log.d("TransaksiVMCancel", "result: ${result.exception}")
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception) {
                e.message?.let {
                    _errorLiveData.postValue(it)
                }
            }
        }
    }

    class Factory(
        private val auth: FirebaseAuth,
        private val repoNotif: FirebaseNotificationRepo,
        private val repoTransaksi: FirebaseTransaksiRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return DetailTransaksiViewModel(auth, repoNotif, repoTransaksi, dispatchers) as T
        }
    }
}