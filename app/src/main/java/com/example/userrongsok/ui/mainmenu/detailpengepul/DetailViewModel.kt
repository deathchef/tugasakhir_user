package com.example.userrongsok.ui.mainmenu.detailpengepul

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Rongsok
import com.example.userrongsok.model.RongsokAdapter
import com.example.userrongsok.repo.firebase.FirebasePengepulRepo
import com.example.userrongsok.repo.firebase.FirebaseStorageRepo
import com.example.userrongsok.util.DispatcherProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class DetailViewModel(
    private val repoPengepul: FirebasePengepulRepo,
    private val repoStorage: FirebaseStorageRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {

    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _dataRongsokLiveData = MutableLiveData<MutableList<RongsokAdapter>>()
    val dataRongsokLiveData: LiveData<MutableList<RongsokAdapter>> get() = _dataRongsokLiveData

    fun getDataRongsok(id: String) {
        val dataRongsokAdapter = mutableListOf<RongsokAdapter>()
        var allDataRongsok = mutableListOf<Rongsok>()

        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoPengepul.getDataRongsok(id)) {
                    is RepositoryResult.Success -> {
                        Log.d("Get List Rongsok", "result: ${result.data}")
                        allDataRongsok = result.data
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        Log.e("Error Get List", "result: ${result.exception}")
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        Log.d("Cancel Get List", "result: ${result.exception}")
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
                allDataRongsok.forEach {
                    val fotoRongsokUri =
                        repoStorage.downloadFotoRongsok(it.id_toko, it.nama_rongsok)
                    Log.d("Foto Rongsok", fotoRongsokUri.path!!)
                    val rongsokAdapter = RongsokAdapter(
                        it.foto_rongsok,
                        fotoRongsokUri,
                        it.harga_rongsok,
                        it.jenis_rongsok,
                        it.nama_rongsok,
                        it.id_toko,
                        it.isReady
                    )
                    Log.d("Data Rongsok Adapter", rongsokAdapter.toString())
                    dataRongsokAdapter.add(rongsokAdapter)
                }
                CoroutineScope(dispatchers.main()).launch {
                    _dataRongsokLiveData.value = dataRongsokAdapter
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    _errorLiveData.value = e.message!!
                }
            }
        }
    }


    class Factory(
        private val repoPengepul: FirebasePengepulRepo,
        private val repoStorage: FirebaseStorageRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return DetailViewModel(repoPengepul, repoStorage, dispatchers) as T
        }
    }
}