package com.example.userrongsok.ui.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.repo.firebase.FirebaseAuthRepo
import com.example.userrongsok.repo.firebase.FirebaseUserRepo
import com.example.userrongsok.util.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.lang.Exception

class LoginViewModel(
    private val auth: FirebaseAuth,
    private val repoAuth: FirebaseAuthRepo,
    private val userRepo: FirebaseUserRepo,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _successLiveData = MutableLiveData<String>()
    val successLiveData: LiveData<String> get() = _successLiveData

    fun loginFirebase(email: String, password: String) {
        CoroutineScope(dispatcher.io()).launch {
            try {
                when (val result = repoAuth.login(email, password)) {
                    is RepositoryResult.Success -> {
                        withContext(dispatcher.main()) {
                            _successLiveData.value = "Berhasil Login"
                        }
                    }
                    is RepositoryResult.Error -> withContext(dispatcher.main()) {
                        _errorLiveData.value = result.exception.message
                    }
                    is RepositoryResult.Canceled -> withContext(dispatcher.main()) {
                        _errorLiveData.value = result.exception?.message
                    }
                }
            } catch (e: Exception) {
                withContext(dispatcher.main()) {
                    _errorLiveData.value = e.message
                }
            }
        }
    }

//    fun getUserFromFirestore(userId: String) {
//        CoroutineScope(dispatcher.io()).launch {
//            try {
//                val tokenInstance = FirebaseInstanceId.getInstance().instanceId.await()
//                when (val result = userRepo.getDataUser(userId)) {
//                    is RepositoryResult.Success -> {
//                        val userData = result.data
//                        if (userData.tokenNotif == "tokenInstance.token") {
//                            userData.tokenNotif = tokenInstance.token
//
//                            userRepo.addUserData(userData)
//                        } else {
//                            userData.tokenNotif = tokenInstance.token
//                        }
//                    }
//                    is RepositoryResult.Error -> withContext(dispatcher.main()) {
//                        _errorLiveData.postValue(result.exception.message)
//                    }
//                    is RepositoryResult.Canceled -> {
//                        withContext(dispatcher.main()) {
//                            result.exception?.message.let {
//                                _errorLiveData.postValue(it)
//                            }
//                        }
//                    }
//                }
//            } catch (e: Exception) {
//                withContext(dispatcher.main()) {
//                    e.message?.let { _errorLiveData.value = it }
//                }
//            }
//        }
//    }

    class Factory(
        private val auth: FirebaseAuth,
        private val authRepo: FirebaseAuthRepo,
        private val userRepo: FirebaseUserRepo,
        private val dispatcher: DispatcherProvider
    ): ViewModelProvider.NewInstanceFactory(){
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoginViewModel(
                auth, authRepo, userRepo, dispatcher
            ) as T
        }
    }
//    suspend fun loginUser(email: String, pass: String) : Map<String, Any> {
//        var result = mutableMapOf<String, Any>(
//            "isSuccess" to false,
//            "message" to "Login Failed"
//        )
//        try {
//            FirebaseAuth.getInstance().signInWithEmailAndPassword(email,pass)
//                .addOnCompleteListener{
//                    if (it.isSuccessful) {
//                        Log.d("ASD", it.getResult()?.user?.uid.toString())
//                        result["isSuccess"] = true
//                    } else {
//                        Log.d("qwe", "Login Failed: ${it.exception?.message}")
//                        result["message"] = "Login Failed: ${it.exception?.message}"
//                    }
//                }.await()
//        } catch (e: Exception) {
//            result["message"] = "Login Failed: ${e.message}"
//        }
//
//        return result
//    }
}