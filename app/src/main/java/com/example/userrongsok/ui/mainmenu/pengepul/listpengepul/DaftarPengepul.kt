package com.example.userrongsok.ui.mainmenu.pengepul.listpengepul

import android.content.Intent
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.userrongsok.R
import com.example.userrongsok.model.Pengepul
import com.example.userrongsok.model.TokoAdapter
import com.example.userrongsok.ui.mainmenu.detailpengepul.DetailPengepul
import kotlinx.android.synthetic.main.activity_daftar_pengepul.*
import kotlinx.android.synthetic.main.item_list_pengepul.*
import org.koin.android.ext.android.inject

class DaftarPengepul : AppCompatActivity() {
    private lateinit var adapter: AdapterPengepul
    private lateinit var viewModel: LIstPengepulViewModel
    private val factory by inject<LIstPengepulViewModel.Factory>()
    private val latitudeUser by lazy {
        intent.getDoubleExtra("latitudeUser",0.0)
    }
    private val longitudeUser by lazy {
        intent.getDoubleExtra("longitudeUser", 0.0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar_pengepul)

        adapter = AdapterPengepul()
        viewModel = ViewModelProvider(this, factory).get(LIstPengepulViewModel::class.java)

        viewModel.errorLiveData.observe(this, onError())
        viewModel.dataTokoLiveData.observe(this, observeGetDaftarPengepul())
//        viewModel.dataPengepul.observe(this, observeGetDataPengepul())


        container_daftarPengepul.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        container_daftarPengepul.adapter = adapter

        adapter.setOnClickListener(object : AdapterPengepul.OnClickListenerCallback {
            override fun onClickListenerCallback(tokoAdapter: TokoAdapter) {
                sendDetail(tokoAdapter)
            }
        })
        showLoading(true)
        viewModel.getDataToko()

        btn_backMap.setOnClickListener {
            onBackPressed()
        }
    }

    private fun sendDetail(tokoAdapter: TokoAdapter) {
        val intent = Intent(this, DetailPengepul::class.java)
        intent.putExtra("dataPengepul", tokoAdapter)
        intent.putExtra("latitudeUser", latitudeUser)
        intent.putExtra("longitudeUser", longitudeUser)
        startActivity(intent).also {
            Log.d("Detail Toko","Result: $tokoAdapter")
            Log.d("LATLNG USER","Lat: $latitudeUser, long: $longitudeUser")
        }
    }

    fun showLoading(state: Boolean) {
        if (state) {
            pb_daftarPengepul.visibility = View.VISIBLE
        } else {
            pb_daftarPengepul.visibility = View.INVISIBLE
        }
    }

    private fun observeGetDaftarPengepul() = Observer<MutableList<TokoAdapter>> { list_pengepul ->
        runOnUiThread {

            if (list_pengepul.isEmpty()) {
                tv_noToko.visibility = View.VISIBLE
                showLoading(false)
            } else {
                adapter.setData(list_pengepul)
                container_daftarPengepul.visibility = View.VISIBLE
                Log.d("Daftar Toko", list_pengepul.toString())
                showLoading(false)
            }
        }
    }

    private fun onError() = Observer<String> {
        runOnUiThread {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }
    }
}