package com.example.userrongsok.ui.mainmenu.pengepul.listpengepul

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.Pengepul
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Toko
import com.example.userrongsok.model.TokoAdapter
import com.example.userrongsok.repo.PengepulRepo
import com.example.userrongsok.repo.firebase.FirebasePengepulRepo
import com.example.userrongsok.repo.firebase.FirebaseStorageRepo
import com.example.userrongsok.util.DispatcherProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class LIstPengepulViewModel(
    private val repoPengepul: FirebasePengepulRepo,
    private val repoStorage: FirebaseStorageRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _dataTokoLiveData = MutableLiveData<MutableList<TokoAdapter>>()
    val dataTokoLiveData: LiveData<MutableList<TokoAdapter>> get() = _dataTokoLiveData

    private val _dataPengepul = MutableLiveData<MutableList<Pengepul>>()
    val dataPengepul: LiveData<MutableList<Pengepul>> get() = _dataPengepul


    fun getDataToko() {
        val dataTokoAdapter = mutableListOf<TokoAdapter>()
        var allDataToko = mutableListOf<Toko>()

        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoPengepul.getDataToko()) {
                    is RepositoryResult.Success -> {
                        Log.d("Get List Toko Pengepul", "result: ${result.data}")
                        allDataToko = result.data
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        Log.e("Error Get List Pengepul", "result: ${result.exception}")
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        Log.e("Error Cancel List", "result: ${result.exception}")
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
                allDataToko.forEach {
                    val fotoTokoUri = repoStorage.downloadFotoToko(it.id_pengepul, it.nama_toko)
                    Log.d("Foto Toko", fotoTokoUri.path!!)
                    val tokoAdapter = TokoAdapter(
                        it.foto_toko,
                        fotoTokoUri,
                        it.nama_toko,
                        it.rating,
                        it.pengunjung,
                        it.id_pengepul,
                        it.latitude,
                        it.longitude,
                        it.address,
                        it.tokenFcm,
                        it.noTelp,
                        it.isOpen
                    )
                    Log.d("Data Toko Adapter", tokoAdapter.toString())
                    dataTokoAdapter.add(tokoAdapter)
                }
                CoroutineScope(dispatchers.main()).launch {
                    _dataTokoLiveData.value = dataTokoAdapter
                }
            } catch (e: Exception) {
                CoroutineScope(dispatchers.main()).launch {
                    _errorLiveData.value = e.message!!
                }
            }
        }
    }

//    fun getDataPengepul(){
//        CoroutineScope(dispatchers.io()).launch {
//            try {
//                when(val result = repoPengepul.getDataPengepul()){
//                    is RepositoryResult.Success->{
//                        _dataPengepul.postValue(result.data)
//                    }
//                    is RepositoryResult.Error -> withContext(dispatchers.main()){
//                        _errorLiveData.postValue("Gagal Memuat Data Pengepul")
//                    }
//                    is RepositoryResult.Canceled -> withContext(dispatchers.main()){
//                        result.exception?.message.let {
//                            _errorLiveData.postValue(it)
//                        }
//                    }
//                }
//            }catch (e: Exception){
//                withContext(dispatchers.main()){
//                    e.message?.let {
//                        _errorLiveData.postValue(it)
//                    }
//                }
//            }
//        }
//    }

    class Factory(
        private val repoPengepul: FirebasePengepulRepo,
        private val repoStorage: FirebaseStorageRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LIstPengepulViewModel(repoPengepul, repoStorage, dispatchers) as T
        }
    }
}