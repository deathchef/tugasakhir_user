package com.example.userrongsok.ui.notification

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.userrongsok.R
import com.example.userrongsok.model.Notifications
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import kotlinx.android.synthetic.main.item_notification_done.view.*
import java.text.SimpleDateFormat
import java.util.*

class NotificationAdapter(val options: FirestoreRecyclerOptions<Notifications>) :
    FirestoreRecyclerAdapter<Notifications, NotificationAdapter.NotificationViewHolder>(options!!) {

    private  var listener: OnClickListener? = null
    fun setOnClickListener(listener: OnClickListener){
        this.listener = listener
    }

    override fun updateOptions(options: FirestoreRecyclerOptions<Notifications>) {
        super.updateOptions(options)
    }


    fun deleteData(position: Int){
        snapshots.getSnapshot(position).reference.delete()
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NotificationAdapter.NotificationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_notification_done, parent, false)
        return NotificationViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: NotificationAdapter.NotificationViewHolder,
        position: Int,
        model: Notifications
    ) {
        holder.bind(model, position)
    }

    inner class NotificationViewHolder(private val v: View) : RecyclerView.ViewHolder(v) {
        fun bind(notification: Notifications, position: Int) {
            fun Date?.parseDate(): String {
                val inputFormat = notification.date
                val date = inputFormat?.toDate()
                val outputFormat = SimpleDateFormat("dd/MM/YYYY", Locale("ID"))
                return outputFormat.format(date)
            }

            v.tv_judulNotification.text = notification.data.get("tittle")
            v.tv_namaUserNotif.text = notification.data.get("nama_pengepul")
            v.tv_typeNotif.text = notification.data.get("type")
            v.tv_tanggalNotif.text = notification.date?.toDate().parseDate()

            listener.let {
                v.setOnClickListener {
                    listener?.onClick(notification, position)
                }
            }
        }
    }

    interface OnClickListener{
        fun onClick(notification: Notifications, position: Int)
    }
}