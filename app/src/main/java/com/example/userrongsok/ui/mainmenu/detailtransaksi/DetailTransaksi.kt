package com.example.userrongsok.ui.mainmenu.detailtransaksi

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.userrongsok.R
import com.example.userrongsok.model.BodyPayloadNotif
import com.example.userrongsok.model.RongsokAdapter
import com.example.userrongsok.model.TokoAdapter
import com.example.userrongsok.services.DataManager
import com.example.userrongsok.ui.mainmenu.MainMenuActivity
import com.example.userrongsok.util.ConstantsApp
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_detail_pengepul.*
import kotlinx.android.synthetic.main.activity_detail_transaksi.*
import org.koin.android.ext.android.inject
import java.util.*


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DetailTransaksi() : AppCompatActivity() {

    lateinit var auth: FirebaseAuth
    lateinit var date: Timestamp

    private lateinit var adapterDetailRongsok: AdapterDetailTransaksi

    private lateinit var viewModel: DetailTransaksiViewModel
    private val factory by inject<DetailTransaksiViewModel.Factory>()

    private var fileUri: Uri? = null
    private lateinit var uriPhoto: Uri
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
//    lateinit var locationRequest: LocationRequest

    private val dataToko by lazy {
        intent.getParcelableExtra<TokoAdapter>("dataPengepul")
    }
    private val dataRongsok: ArrayList<RongsokAdapter> by lazy {
        intent.getParcelableArrayListExtra("dataRongsok")
    }

    private val hargaTotal by lazy {
        intent.getIntExtra("hargaTotal", 0)
    }

    lateinit var locationManager: LocationManager
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var addressUser: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_transaksi)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        auth = FirebaseAuth.getInstance()
        date = Timestamp.now()
        var namaUser = auth.currentUser?.displayName

        val idUser = auth.currentUser?.uid.toString()
        val namaPengepul = dataToko.nama_toko
        val idPengepul = dataToko.id_pengepul
        val tokenFcm = dataToko.tokenFcm
        val noTelpToko = dataToko.noTelp
        adapterDetailRongsok = AdapterDetailTransaksi(this@DetailTransaksi, mutableListOf())

        viewModel = ViewModelProvider(this, factory).get(DetailTransaksiViewModel::class.java)

        viewModel.errorLiveData.observe(this, onError())
        viewModel.successLiveData.observe(this, onSuccess())
//        viewModel.photoLiveData.observe(this, onSuccessDownloadPhoto())
        container_detailTransaksi.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        container_detailTransaksi.adapter = adapterDetailRongsok

        getDataIntent()
        getLocation()

        btn_add_fotoTransaksi.setOnClickListener {

            CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this@DetailTransaksi)
        }

        btn_jualRongsok.setOnClickListener {
            if (fileUri == null) {
                val snackBar = Snackbar.make(it, "Tidak Ada Foto Transaksi", Snackbar.LENGTH_SHORT)
                snackBar.show()
            } else {
                observeAddTransaksi(noTelpToko, idPengepul, namaPengepul)
                observeAddRongsokTransaksi()
                viewModel.uploadFotoTransaksi(namaPengepul, fileUri!!)
                BodyPayloadNotif(
                    BodyPayloadNotif.Data(
                        idPengepul,
                        idUser,
                        namaUser,
                        "$namaUser ingin mengajukan jual rongsok",
                        "Orderan Rongsok",
                        ConstantsApp.SHOP
                    ),
                    date,
                    tokenFcm
                ).also {
                    viewModel.addNotif(it, idPengepul)
                    viewModel.sendNotification(it)
                }

                startActivity(Intent(this, MainMenuActivity::class.java).also {
                    Intent.FLAG_ACTIVITY_CLEAR_TOP
                    Intent.FLAG_ACTIVITY_CLEAR_TASK
                    Intent.FLAG_ACTIVITY_NEW_TASK
                })
                Log.d("foto transaksi", "$fileUri")
            }
        }
    }


    private fun isLocationEnabled(): Boolean {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    @SuppressLint("MissingPermission")
    fun getLocation() {
        var geocoder = Geocoder(this, Locale.getDefault())
        var streetName = ""
        if (permissions()){
            if (isLocationEnabled()) {
                fusedLocationProviderClient.lastLocation.addOnCompleteListener { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        newLocation()
                    } else {
                        Log.d(
                            "Location",
                            "result: lat now: ${location.latitude}, long now: ${location.longitude})"
                        )
                        latitude = location.latitude
                        longitude = location.longitude

                        var address = geocoder.getFromLocation(latitude,longitude,1)
                        streetName = address.get(0).getAddressLine(0)

                        addressUser = streetName
                        tv_alamatPelanggan.text = streetName
//                        tv_namaKota.text = cityName
                    }
                }
            } else {
                Toast.makeText(this, "Hidupkan GPS Handphone anda", Toast.LENGTH_LONG).show()
            }
        }else{
            Toast.makeText(this,"Setujui Perizinan Layanan", Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("MissingPermission")
    private fun newLocation() {
        var locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 0
        locationRequest.fastestInterval = 0
        locationRequest.numUpdates = 1
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient!!.requestLocationUpdates(
            locationRequest, locationCallback, Looper.myLooper()
        )
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(p0: LocationResult?) {
            var lastLocation: Location = p0!!.lastLocation
//            tv_alamatPelanggan.text = getCityName(lastLocation.latitude, lastLocation.longitude)
            latitude = lastLocation.latitude
            longitude = lastLocation.longitude

            Log.d("lat Last", "result: $latitude")
            Log.d("long last", "result: $longitude")
        }
    }

    fun observeAddRongsokTransaksi() {
        var nama_rongsok: String
        var foto_rongsok: String
        var harga_rongsok: Int
        var id_user = auth.currentUser?.uid!!
        var quantity_rongsok: Int
        dataRongsok.forEach {
            nama_rongsok = it.nama_rongsok
            foto_rongsok = it.foto_rongsok
            harga_rongsok = it.harga_rongsok
            quantity_rongsok = it.quantity
            viewModel.addRongsok(
                foto_rongsok,
                nama_rongsok,
                id_user,
                dataToko.id_pengepul,
                harga_rongsok,
                quantity_rongsok
            )
            Log.d(
                "Data item Transaksi",
                "result: $foto_rongsok /n $nama_rongsok /n $id_user, ${dataToko.id_pengepul}, /n $quantity_rongsok"
            )
        }
    }

    fun observeAddTransaksi(noTelp: String, id_toko: String, nama_toko_pengepul: String) {
        val auth = FirebaseAuth.getInstance()
        val catatan = et_catatanTransaksi.text.toString()
        val id_pengepul = id_toko
        val id_user = auth.currentUser?.uid.toString()
        val isDone: Boolean = false
        val isProccess: Boolean = false
        val isCancel: Boolean = false
        val isRated: Boolean = false
        var status = "Menunggu Konfirmasi"
        val totalHarga = hargaTotal
        val nama_pelanggan = auth.currentUser?.displayName.toString()
        val foto_transaksi = "/transaksi$id_user${dataToko.nama_toko}"
        val nama_toko = nama_toko_pengepul
        val latitude = latitude
        val longitude = longitude
        var address = addressUser
        val tanggal_transaksi = Timestamp.now()
        viewModel.addTransaksi(
            catatan,
            foto_transaksi,
            id_pengepul,
            id_user,
            isDone,
            isProccess,
            isCancel,
            isRated,
            status,
            totalHarga,
            nama_pelanggan,
            nama_toko,
            latitude,
            longitude,
            address,
            noTelp,
            tanggal_transaksi
        )
        Log.d(
            "Data Transaksi",
            "result: ${catatan}/n $id_pengepul, $id_user /n $totalHarga /n $nama_pelanggan, $nama_toko /n $tanggal_transaksi /n $noTelp"
        )
    }

    fun onSuccess() = Observer<String> {
        this.runOnUiThread {
            Toast.makeText(this@DetailTransaksi, it, Toast.LENGTH_LONG).show()
        }

    }

    fun onError() = Observer<String> {
        this.runOnUiThread {
            Toast.makeText(this@DetailTransaksi, it, Toast.LENGTH_SHORT).show()
        }
    }

    fun getDataIntent() {
        if (dataRongsok.isEmpty()) {
            Toast.makeText(this, "Tidak Ada Data", Toast.LENGTH_SHORT).show()
        } else {
            container_detailTransaksi?.adapter?.let { a ->
                if (a is AdapterDetailTransaksi) {
                    a.setData(dataRongsok)
                }
            }
        }
        tv_totalHarga.text = hargaTotal.toString()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val result = CropImage.getActivityResult(data)
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                fileUri = result.uri
                Log.d("Phtoto Transaksi: ", fileUri?.path.toString())
                Glide.with(this@DetailTransaksi).load(fileUri).into(iv_foto_rongsok_transaksi)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Log.e("Pict Toko: ", error.toString())
            }
        }
    }

    fun permissions(): Boolean {
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    Log.d("PERMISSION", "CHECK")
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {
                    Log.d("PERMISSION", "RATIONAL")
                }
            }).check()
        return true
    }
}