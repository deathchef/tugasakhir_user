package com.example.userrongsok.ui.notification

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.userrongsok.R
import com.example.userrongsok.model.Notifications
import com.example.userrongsok.repo.firebase.FirebaseNotificationRepo
import com.example.userrongsok.ui.chat.ActivityChat
import com.example.userrongsok.ui.mainmenu.transaksi.TransaksiFragment
import com.example.userrongsok.ui.mainmenu.transaksi.detailtransaksinot.DetailTransaksiNotification
import com.example.userrongsok.util.ConstantsApp
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_notification.*
import org.koin.android.ext.android.inject

class NotificationActivity : AppCompatActivity() {
    private lateinit var viewModel: NotificationViewModel
    private val factory by inject<NotificationViewModel.Factory>()
    private var adapterrNotif: NotificationAdapter? = null
    lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        auth = FirebaseAuth.getInstance()
        viewModel = ViewModelProvider(this, factory).get(NotificationViewModel::class.java)
        viewModel.errorLiveData.observe(this, onError())
    }

    override fun onStart() {
        super.onStart()
        initRecycleView()
    }


    override fun onStop() {
        super.onStop()
        adapterrNotif?.stopListening()
    }

    private fun initRecycleView() {
        container_notifikasi.layoutManager = LinearLayoutManager(this)
        container_notifikasi.setHasFixedSize(true)

        viewModel.getDataNotif()
        viewModel.dataNotif.observe(this, Observer {
            val options = FirestoreRecyclerOptions.Builder<Notifications>()
                .setQuery(it, Notifications::class.java).build()

            adapterrNotif = NotificationAdapter(options = options)
            container_notifikasi.adapter = adapterrNotif
            adapterrNotif?.startListening()
            adapterrNotif?.setOnClickListener(object : NotificationAdapter.OnClickListener {
                override fun onClick(notification: Notifications, position: Int) {
                    adapterrNotif?.deleteData(position)
                    if (notification.data.get("type").toString() == ConstantsApp.SHOP) {
//                        var intent = Intent(this@NotificationActivity, TransaksiFragment::class.java)
//                        intent.putExtra("notif","key")
//                        start
                    } else if (notification.data.get("type").toString() == ConstantsApp.CHAT) {
                        startActivity(Intent(this@NotificationActivity, ActivityChat::class.java))
                    }
                }

            })
        })
        ItemTouchHelper(object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                adapterrNotif?.deleteData(viewHolder.adapterPosition)
            }
        }).attachToRecyclerView(container_notifikasi)
    }

    private fun onError() = Observer<String> {

    }
//
//    fun showLoading(state: Boolean) {
//        if (state) {
//            pb_notifikasi.visibility = View.VISIBLE
//        } else {
//            pb_notifikasi.visibility = View.INVISIBLE
//        }
//    }
}