package com.example.userrongsok.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.R
import com.example.userrongsok.admin.MenuAdmin
import com.example.userrongsok.ui.mainmenu.MainMenuActivity
import com.example.userrongsok.ui.regis.RegisActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import kotlin.coroutines.CoroutineContext

class LoginActivity : AppCompatActivity() {
    lateinit var auth: FirebaseAuth
    private lateinit var viewModel: LoginViewModel
    private val factory: LoginViewModel.Factory by inject()
    private val LOGIN = LoginActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        viewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)

        viewModel.errorLiveData.observe(this, onError())
        viewModel.successLiveData.observe(this, onSuccess())

        tv_toRegis.setOnClickListener {
            startActivity(Intent(this, RegisActivity::class.java))
        }

        //fungsi login

        btn_login.setOnClickListener {
            showLoading(true)
            if (et_emailLogin.text.toString() == "admin@rongsok.com" && et_passwordLogin.text.toString() == "admin1234") {
                onAdmin()
            } else {
                loginUser()
            }
            Log.d(LOGIN, "Info Login : email(${et_emailLogin.text.toString()})")
        }
    }

    private fun loginUser() {
        var email = et_emailLogin.text.toString().trim() { it <= ' ' }
        var password = et_passwordLogin.text.toString().trim() { it <= ' ' }
        when {
            email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                showError("Email Tidak Valid atau Kosong")
                showLoading(false)
            }
            password.isEmpty() -> {
                showError("Password Tidak Valid")
                showLoading(false)
            }
            else -> {
                showLoading(false)
                viewModel.loginFirebase(email, password)
            }
        }
    }

    fun onError() = Observer<String> {
        runOnUiThread {
            showError(it)
            showLoading(false)
        }
    }

    private fun onSuccess() = Observer<String> { it ->
        runOnUiThread {
            startActivity(Intent(this, MainMenuActivity::class.java)).apply {
                Intent.FLAG_ACTIVITY_CLEAR_TOP
                Intent.FLAG_ACTIVITY_CLEAR_TASK
                Intent.FLAG_ACTIVITY_NEW_TASK
            }
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            finish()
        }
    }

    private fun onAdmin() {
        showLoading(false)
        startActivity(Intent(this, MenuAdmin::class.java)).apply {
            Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
        finish()
    }

    fun showError(msg: String) {
        val snackbar = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.errorMsg))
        snackbar.setTextColor(ContextCompat.getColor(this, R.color.teksPutih))
        snackbar.show()
    }

    fun showLoading(state: Boolean) {
        if (state) {
            pb_login.visibility = View.VISIBLE
        } else {
            pb_login.visibility = View.INVISIBLE
        }
    }
}