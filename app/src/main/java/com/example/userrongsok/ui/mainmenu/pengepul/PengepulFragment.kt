package com.example.userrongsok.ui.mainmenu.pengepul

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.R
import com.example.userrongsok.model.Toko
import com.example.userrongsok.model.Transaksi
import com.example.userrongsok.ui.mainmenu.pengepul.listpengepul.DaftarPengepul
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.fragment_pengepul.*
import org.koin.android.ext.android.inject

class PengepulFragment : Fragment(), OnMapReadyCallback {

    private lateinit var viewModel: DataTransaksiViewModel
    private val factory by inject<DataTransaksiViewModel.Factory>()

    private lateinit var map: GoogleMap
//    private lateinit var geofencingMap: GeofencingClient

    var mutableList = mutableListOf<Transaksi>()
    var toko = mutableListOf<Toko>()

    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var locationRequest: LocationRequest
    lateinit var locationCallback: LocationCallback

    var latitudeUser = 0.0
    var longitudeUser = 0.0

    private lateinit var lastLocation: Location
    private var marker: Marker? = null
//    private val placeUII = LatLng(-7.6856648,110.4122754)

    @SuppressLint("MissingPermission")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mapPengepul.onCreate(savedInstanceState)
        mapPengepul.onResume()

        mapPengepul.getMapAsync(this)
    }


    private fun buildLocationCallback(googleMap: GoogleMap) {
        map = googleMap
//        geofencingMap = LocationServices.getGeofencingClient(this@PengepulFragment.requireActivity())
//
//        val circleOption = CircleOptions()
//            .center(placeUII)
//            .radius(3000.0)
//            .strokeColor(Color.argb(255,255,0,0))
//            .fillColor(Color.argb(64,255,0,0))
//            .strokeWidth(4F)
//        map.addCircle(circleOption)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                lastLocation = p0!!.locations.get(p0!!.locations.size - 1)

                if (marker != null) {
                    marker!!.remove()
                }
                latitudeUser = lastLocation.latitude
                longitudeUser = lastLocation.longitude

                val placeUser = LatLng(latitudeUser, longitudeUser)
//                val markerOptions = MarkerOptions()
//                    .position(placeUser)
//                    .title("Posisi Anda")
//                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))

                this@PengepulFragment.toko.forEach {
                    val pengepulPlace = LatLng(it.latitude, it.longitude)
                    val markerOptionPengepul = MarkerOptions()
                        .position(pengepulPlace)
                        .title(it.nama_toko)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                    map!!.addMarker(markerOptionPengepul)
//                marker = map!!.addMarker(markerOptions)

                    //Camera Map
                    map!!.moveCamera(CameraUpdateFactory.newLatLng(placeUser))
                    map!!.animateCamera(CameraUpdateFactory.zoomTo(11f))
                }
            }
        }
    }

    private fun buildLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 3000
        locationRequest.smallestDisplacement = 11f
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pengepul, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, factory).get(DataTransaksiViewModel::class.java)
        viewModel.errorLiveData.observe(viewLifecycleOwner, onError())
        viewModel.dataTransaksi.observe(viewLifecycleOwner, observeDataTransaksi())
        viewModel.dataPengepul.observe(viewLifecycleOwner, observeDataPengepul())

        viewModel.getDataTransaksi()
        viewModel.getListPengepul()
    }

    @SuppressLint("MissingPermission")
    private fun observeDataPengepul() = Observer<MutableList<Toko>> {
        activity?.runOnUiThread {
            this.toko = it

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                buildLocationRequest()
                buildLocationCallback(map)
            }

            fusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(this@PengepulFragment.requireContext())
            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.myLooper()
            )
        }
    }

    private fun onError() = Observer<String> {
        Log.d("Fail Get Data", "result: $it")
    }

    private fun observeDataTransaksi() = Observer<MutableList<Transaksi>> { list_transaksi ->
        activity?.runOnUiThread {
            this.mutableList = list_transaksi

            if (mutableList.isEmpty()) {
                btn_list.setOnClickListener {
                    val intent = Intent(
                        this@PengepulFragment.requireContext(),
                        DaftarPengepul::class.java
                    )
                    intent.putExtra("latitudeUser", latitudeUser)
                    intent.putExtra("longitudeUser", longitudeUser)
                    startActivity(intent)
                    Log.d("LATLNG USER", "lat: $latitudeUser, long: $longitudeUser")
                }
            } else {
                btn_list.setOnClickListener {
                    Toast.makeText(
                        this@PengepulFragment.requireContext(),
                        "Transaksi Sedang Berlangsung",
                        Toast.LENGTH_LONG
                    ).show()
                }
                Log.d("Transaksi Berlangsung", "result: $mutableList")
            }
        }
    }

    override fun onMapReady(p0: GoogleMap?) {
        p0?.let {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            map = it
            map.isMyLocationEnabled = true
            map.uiSettings.isZoomControlsEnabled = true
        }
    }

    override fun onStop() {
        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(this@PengepulFragment.requireContext())
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        super.onStop()
    }
}