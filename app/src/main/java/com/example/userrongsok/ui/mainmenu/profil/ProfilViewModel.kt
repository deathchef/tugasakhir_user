package com.example.userrongsok.ui.mainmenu.profil

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Users
import com.example.userrongsok.repo.firebase.FirebaseUserRepo
import com.example.userrongsok.util.DispatcherProvider
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.core.Repo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class ProfilViewModel(
    private val repoUser: FirebaseUserRepo,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _dataUser = MutableLiveData<Users>()
    val dataUser: LiveData<Users> get() = _dataUser

    private val _successLiveData = MutableLiveData<String>()
    val successLiveData: LiveData<String> get() = _successLiveData

//    private val _usernameLiveData = MutableLiveData<String>()
//    val usernameLiveData: LiveData<String> = _usernameLiveData

    fun getDataUser(userId: String) {
        CoroutineScope(dispatcher.io()).launch {
            try {
                when (val result = repoUser.getDataUser(userId)) {
                    is RepositoryResult.Success -> {
                        _dataUser.postValue(result.data)
                    }
                    is RepositoryResult.Error -> withContext(dispatcher.main()) {
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> withContext(dispatcher.main()) {
                        result.exception?.message.let { _errorLiveData.postValue(it) }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatcher.main()) {
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                }
            }
        }
    }

    fun updateProfil(nama: String, nomor_handphone: String){
        CoroutineScope(dispatcher.io()).launch {
            try {
                when(val result = repoUser.updateProfil(nama, nomor_handphone)){
                    is RepositoryResult.Success-> {
                        _successLiveData.postValue("Data Berhasil Diperbaharui")
                    }
                    is RepositoryResult.Error -> withContext(dispatcher.main()){
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> withContext(dispatcher.main()){
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            }catch (e: Exception){
                withContext(dispatcher.main()){
                    e.message?.let { _errorLiveData.postValue(it) }
                }
            }
        }
    }

    class Factory(
        private val repoUser: FirebaseUserRepo,
        private val dispatcher: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ProfilViewModel(repoUser, dispatcher) as T
        }
    }
}