package com.example.userrongsok.ui.regis

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Users
import com.example.userrongsok.repo.firebase.FirebaseAuthRepo
import com.example.userrongsok.util.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.lang.Exception

class RegisterViewModel(
    private val auth: FirebaseAuth,
    private val authRepo: FirebaseAuthRepo,
    private val dispatcher: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _successLiveData = MutableLiveData<String>()
    val successLiveData: LiveData<String> get() = _successLiveData

    fun registerFirebase(nama: String, email: String, password: String, phone: String, tokenFcm: String) {
        CoroutineScope(dispatcher.main()).launch {
            try {
                when (val result = authRepo.register(nama, email, password, phone, tokenFcm)) {
                    is RepositoryResult.Success -> {
                        _successLiveData.value = "Berhasil Mendaftar"
                    }
                    is RepositoryResult.Error -> {
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> {
                        _errorLiveData.postValue(result.exception?.message)
                    }
                }
            } catch (e: Exception) {
                _errorLiveData.postValue(e.message)
            }
        }
    }
//
//    fun saveToDatabase(nama: String, tokenFcm: String){
//        CoroutineScope(dispatcher.main()).launch {
//            try {
//                when(val result = authRepo.saveDatabase(nama, tokenFcm)){
//                    is RepositoryResult.Success ->{
//                        _successLiveData.postValue(" ")
//                    }
//                    is RepositoryResult.Error->{
//                        _errorLiveData.postValue(result.exception.message)
//                    }
//                    is RepositoryResult.Canceled->{
//                        _errorLiveData.postValue(result.exception?.message)
//                    }
//                }
//            }catch (e: Exception){
//                _errorLiveData.postValue(e.message)
//            }
//        }
//    }

    class Factory(
        private val auth: FirebaseAuth,
        private val authRepo: FirebaseAuthRepo,
        private val dispatcher: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RegisterViewModel(
                auth, authRepo, dispatcher
            ) as T
        }
    }
//    suspend fun registerUser(name: String, email: String, pass: String, phone: String) : Map<String, Any> {
//        var result = mutableMapOf<String, Any>(
//            "isSuccess" to false,
//            "message" to "Registration Failed"
//        )
//        try {
//            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,pass)
//                .addOnCompleteListener{
//                    if (it.isSuccessful) {
//                        Log.d("ASD", it.getResult()?.user?.uid.toString())
//                        result["isSuccess"] = true
//                    } else {
//                        Log.d("qwe", "Registration Failed: ${it.exception?.message}")
//                        result["message"] = "Registration Failed: ${it.exception?.message}"
//                    }
//                }.await()
//        } catch (e: Exception) {
//            result["message"] = "Registration Failed: ${e.message}"
//        }
//
//        return result
//    }
}