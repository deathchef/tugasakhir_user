package com.example.userrongsok.ui.mainmenu.detailpengepul

import android.content.Intent
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.userrongsok.R
import com.example.userrongsok.model.RongsokAdapter
import com.example.userrongsok.model.TokoAdapter
import com.example.userrongsok.ui.mainmenu.detailtransaksi.DetailTransaksi
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_detail_pengepul.*
import kotlinx.android.synthetic.main.bottomsheet_add_rongsok.*
import org.koin.android.ext.android.inject
import java.util.ArrayList

class DetailPengepul : AppCompatActivity() {
    private lateinit var adapterRongsok: AdapterListRongsok
    private lateinit var viewModel: DetailViewModel

    private val factory by inject<DetailViewModel.Factory>()
    private var listRongsokAdapter = ArrayList<RongsokAdapter>()
    private var totalHarga: Int = 0

    lateinit var locationUser: Location
    lateinit var locationPengepul: Location

    private val data by lazy {
        intent.getParcelableExtra<TokoAdapter>("dataPengepul")
    }
    private val latitudeUser by lazy {
        intent.getDoubleExtra("latitudeUser", 0.0)
    }
    private val longitudeUser by lazy {
        intent.getDoubleExtra("longitudeUser", 0.0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pengepul)

        locationPengepul = Location("locationPengepul")
        locationUser = Location("locationUser")

        adapterRongsok = AdapterListRongsok(this, arrayListOf())
        viewModel = ViewModelProvider(this, factory).get(DetailViewModel::class.java)

        viewModel.errorLiveData.observe(this, onError())
        viewModel.dataRongsokLiveData.observe(this, observeGetDataRongsok())

        container_daftarRongsok.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        container_daftarRongsok.adapter = adapterRongsok
        showLoading(true)
        viewModel.getDataRongsok(data.id_pengepul)
        getDataIntent()

        adapterRongsok.setOnClickListener(object : AdapterListRongsok.OnClickListenerCallback {
            override fun addItem(rongsokAdapter: RongsokAdapter) {
                incrementQuantityProduct(rongsokAdapter)

                if (rongsokAdapter.quantity >= 1) {
                    totalHarga = totalHarga + rongsokAdapter.harga_rongsok
                    tv_harga.text = totalHarga.toString()
                }

                Log.d("plus Item", "jumlah: ${rongsokAdapter.quantity}")
                Log.d("rongsok", "result: $rongsokAdapter")
                Log.d("hargaTotal", "result: $totalHarga")

            }

            override fun minItem(rongsok: RongsokAdapter) {
                decrementQuantityProduct(rongsok)

                if (rongsok.quantity >= 0) {
                    totalHarga = totalHarga - rongsok.harga_rongsok
                    tv_harga.text = totalHarga.toString()
                }

                Log.d("minus Item", "Jumlah : ${rongsok.quantity}")
                Log.d("rongsok", "result: $rongsok")
                Log.d("hargaTotal", "result: ${totalHarga}")
            }
        })
        btn_addRongsok.setOnClickListener {
            if (tv_harga.text.toString().isNullOrEmpty() || tv_harga.text.toString() == "0") {
                Toast.makeText(this, "Tidak ada item rongsok di keranjang", Toast.LENGTH_LONG)
                    .show()
            } else {
                val intent = Intent(this, DetailTransaksi::class.java)
                intent.putExtra("dataPengepul", data)
                intent.putParcelableArrayListExtra("dataRongsok", listRongsokAdapter)
                intent.putExtra("hargaTotal", totalHarga)
                startActivity(intent)
                Log.d("Activity", "Move to Detail Transaksi")
                Log.d("dataPengepul", "result dataPengepul: $data")
                Log.d("dataRongsok", "result listRongsok: $listRongsokAdapter")
                Log.d("totalHarga", "result totalHarga: $totalHarga")
            }
        }
    }

    fun incrementQuantityProduct(product: RongsokAdapter) {

        var tempSelectedProducts: ArrayList<RongsokAdapter> = if (listRongsokAdapter.size == null) {
            arrayListOf()
        } else {
            listRongsokAdapter as ArrayList<RongsokAdapter>
        }
        val sameProduct: RongsokAdapter? = tempSelectedProducts.find { p ->
            p.nama_rongsok == product.nama_rongsok
        }
        sameProduct?.let {
//            it.quantity = it.quantity + 1
        } ?: kotlin.run {
            listRongsokAdapter.add(product)
        }
    }

    fun decrementQuantityProduct(product: RongsokAdapter) {

        val tempSelectedProducts: ArrayList<RongsokAdapter> = if (listRongsokAdapter.size == null) {
            arrayListOf()
        } else {
            listRongsokAdapter as ArrayList<RongsokAdapter>
        }
        val sameProduct: RongsokAdapter? = tempSelectedProducts.find { p ->
            p.nama_rongsok == product.nama_rongsok
        }
        sameProduct?.let {
//            it.quantity = it.quantity - 1
            if (it.quantity == 0 || it.quantity <= 0) {
                listRongsokAdapter.remove(it)
            }
        } ?: kotlin.run {
            listRongsokAdapter.add(product)
        }
    }

    fun observeGetDataRongsok() = Observer<MutableList<RongsokAdapter>> { list_rongsok ->
        runOnUiThread {
            if (list_rongsok.isEmpty()) {
                Toast.makeText(this, "Gagal Memuat Data Rongsok", Toast.LENGTH_LONG).show()
                showLoading(false)
            } else {
                adapterRongsok.setData(list_rongsok)
                container_daftarRongsok.isVisible = true
                Log.d("Get Data Rongsok", "result: $list_rongsok")
                showLoading(false)
            }
        }
    }


    fun getDataIntent() {
        runOnUiThread {
            var ratingToko = data.rating / data.pengunjung
//            Glide.with(this)
//                .load(data.foto_tokoUri)
//                .into(iv_fotoToko)
            tv_namaPengepul.text = data.nama_toko
            tv_ratingToko.text = ratingToko.toString().subSequence(0, 3)
            tv_alamatToko.text = data.address

            locationPengepul.latitude = data.latitude
            locationPengepul.longitude = data.longitude

            locationUser.latitude = latitudeUser
            locationUser.longitude = longitudeUser

            Log.d("Latlng Pengepul", "lat: ${data.latitude}, long: ${data.longitude}")
            Log.d("Latlng User", "lat: $latitudeUser, long: $longitudeUser")

            var jarak = locationUser.distanceTo(locationPengepul) / 1000
            tv_jarak.text = jarak.toString().subSequence(0, 4)

        }
    }

    fun onError() = Observer<String> {
        runOnUiThread {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }
    }

    fun showLoading(state: Boolean) {
        if (state) {
            pb_detailPengepul.visibility = View.VISIBLE
        } else {
            pb_detailPengepul.visibility = View.GONE
        }
    }
}