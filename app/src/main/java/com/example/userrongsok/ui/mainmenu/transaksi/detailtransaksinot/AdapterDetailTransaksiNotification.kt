package com.example.userrongsok.ui.mainmenu.transaksi.detailtransaksinot

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.userrongsok.R
import com.example.userrongsok.model.RongsokAdapter
import com.example.userrongsok.model.RongsokTransaksi
import com.example.userrongsok.model.RongsokTransaksiAdapter
import com.example.userrongsok.model.Transaksi
import kotlinx.android.synthetic.main.item_detail_rongsok.view.*
import java.util.ArrayList

class AdapterDetailTransaksiNotification(
    val context: Context,
    var listRongsok: ArrayList<RongsokTransaksiAdapter>
) : RecyclerView.Adapter<AdapterDetailTransaksiNotification.listTransaksiHolder>() {

    inner class listTransaksiHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(rongsok: RongsokTransaksiAdapter) {
            itemView.tv_nama_rongsokDetail.text = rongsok.nama_rongsok
            itemView.tv_beratRongsok.text = rongsok.quantity.toString()
            itemView.tv_harga_rongsok_transaksi.text = rongsok.harga_rongsok.toString()
            Glide.with(itemView.context)
                .load(rongsok.foto_rongsokUri)
                .into(itemView.iv_fotoDetailRongsok)
        }
    }

    fun setData(listRongsok: List<RongsokTransaksiAdapter>) {
        this.listRongsok.clear()
        this.listRongsok.addAll(listRongsok)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDetailTransaksiNotification.listTransaksiHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_detail_rongsok, parent, false)
        return listTransaksiHolder(view)
    }

    override fun onBindViewHolder(holder: listTransaksiHolder, position: Int) {
        holder.bind(listRongsok[position])
    }

    override fun getItemCount(): Int {
        return listRongsok.size
    }
}