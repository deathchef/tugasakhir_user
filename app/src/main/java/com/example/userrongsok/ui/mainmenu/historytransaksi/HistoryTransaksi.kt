package com.example.userrongsok.ui.mainmenu.historytransaksi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.userrongsok.R
import com.example.userrongsok.model.Transaksi
import kotlinx.android.synthetic.main.activity_history_transaksi.*
import org.koin.android.ext.android.inject

class HistoryTransaksi : AppCompatActivity() {
    private lateinit var viewModel: HistoryViewModel
    private val factory by inject<HistoryViewModel.Factory>()

    private lateinit var adapter: AdapterHistory
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_transaksi)

        adapter = AdapterHistory(this)
        viewModel = ViewModelProvider(this, factory).get(HistoryViewModel::class.java)

        viewModel.errorLiveData.observe(this, onError())
        viewModel.dataHistoryTransaksi.observe(this, observeDataHistroy())

        container_history.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        container_history.adapter = adapter
        showLoading(true)
        viewModel.getDataHistory()
    }

    private fun showLoading(state: Boolean) {
        if (state){
            pb_history.visibility = View.VISIBLE
        }else{
            pb_history.visibility = View.GONE
        }
    }

    private fun onError() = Observer<String> {
        Toast.makeText(this, it, Toast.LENGTH_LONG).show()
    }

    private fun observeDataHistroy() = Observer<MutableList<Transaksi>> {list_history->
        runOnUiThread {
            if (list_history.isEmpty()){
                ll_history.visibility = View.VISIBLE
                showLoading(false)
            }else{
                adapter.setData(list_history)
                container_history.visibility = View.VISIBLE
                showLoading(false)
            }
        }
    }
}