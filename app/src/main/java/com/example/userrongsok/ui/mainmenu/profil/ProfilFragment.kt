package com.example.userrongsok.ui.mainmenu.profil

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.R
import com.example.userrongsok.model.Users
import com.example.userrongsok.ui.login.LoginActivity
import com.example.userrongsok.ui.mainmenu.tentang.Tentang
import com.example.userrongsok.util.FirebaseHelper
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.dialog_edit_user.*
import kotlinx.android.synthetic.main.dialog_edit_user.view.*
import kotlinx.android.synthetic.main.fragment_profil.*
import org.koin.android.ext.android.inject

class ProfilFragment : Fragment() {

    lateinit var auth: FirebaseAuth
    private lateinit var viewModel: ProfilViewModel
    private val factory by inject<ProfilViewModel.Factory>()
    private lateinit var user: Users
    private val firebaseHelper by inject<FirebaseHelper>()
    private lateinit var showDialog: AlertDialog
    private val dialogEditUser by lazy {
        layoutInflater.inflate(R.layout.dialog_edit_user, null, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profil, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        auth = FirebaseAuth.getInstance()

        viewModel = ViewModelProvider(this, factory).get(ProfilViewModel::class.java)
        viewModel.errorLiveData.observe(viewLifecycleOwner, onError())
        viewModel.dataUser.observe(viewLifecycleOwner, observeGetDataUser())
        viewModel.successLiveData.observe(viewLifecycleOwner, onSuccess)
//        viewModel.usernameLiveData.observe(viewLifecycleOwner, onSuccessUpdateUsername)

        viewModel.getDataUser(auth.currentUser?.uid.toString())

        btn_logout.setOnClickListener {
            firebaseHelper.firebaseSignOut()
            startActivity(Intent(activity, LoginActivity::class.java)).apply {
                Intent.FLAG_ACTIVITY_CLEAR_TOP
                Intent.FLAG_ACTIVITY_NEW_TASK
            }
            Toast.makeText(requireContext(), "Berhasil Logout", Toast.LENGTH_SHORT).show()
            activity?.finish()
        }
        btn_edit.setOnClickListener {
            this.let {
                if (dialogEditUser.parent != null) {
                    val viewGroup = dialogEditUser.parent as ViewGroup
                    viewGroup.removeView(dialogEditUser)
                }
                val dialog = AlertDialog.Builder(requireContext())
                dialog.setView(dialogEditUser)
                showDialog = dialog.create()
                showDialog.show()

                val nama = dialogEditUser.et_edit_nama_user
                val nomorHandphone = dialogEditUser.et_edit_no_hp

                dialogEditUser.btn_simpan_edit.setOnClickListener {
                    if (dialogEditUser.et_edit_nama_user?.text.toString().isNullOrEmpty()) {
                        dialogEditUser.et_edit_nama_user?.error = "Nama User Tidak Boleh Kosong"
                        dialogEditUser.et_edit_nama_user?.isFocusable = true
                    } else if (dialogEditUser.et_edit_no_hp?.text.toString().isNullOrEmpty()) {
                        dialogEditUser.et_edit_no_hp?.error = "Nomor Handphone Tidak Boleh Kosong"
                        dialogEditUser.et_edit_no_hp?.isFocusable = true
                    } else {
                        Log.d("edit","result: ${nama.text} dan ${nomorHandphone.text}")
                        viewModel.updateProfil(nama?.text.toString(), nomorHandphone?.text.toString())
                        showDialog.dismiss()
                    }
                    viewModel.getDataUser(auth.currentUser?.uid.toString())
                }
                dialogEditUser.btn_batal_edit.setOnClickListener {
                    showDialog.cancel()
                }
            }
        }

        ll_about.setOnClickListener {
            startActivity(Intent(requireContext(), Tentang::class.java))
        }
    }

    private fun observeGetDataUser() = Observer<Users> { data ->
        this.user = data
        if (data == null) {
            Toast.makeText(requireContext(), "Gagal Memuat Data", Toast.LENGTH_SHORT).show()
        } else {
            tv_username.text = data.name
            tv_phone.text = data.phone
            tv_email.text = auth.currentUser?.email
        }
    }

    private fun onError() = Observer<String> {
        activity?.runOnUiThread {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            Log.d(ProfilFragment::class.java.simpleName, "Error : $it")
        }
    }
//    private val onSuccessUpdateUsername = Observer<String> {username->
//        dialogEditUser.et_edit_nama_user?.text.toString()
//    }

    private val onSuccess = Observer<String> {
        Toast.makeText(this@ProfilFragment.requireContext(), it, Toast.LENGTH_LONG).show()
    }
}