package com.example.userrongsok.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.userrongsok.R
import com.example.userrongsok.ui.login.LoginActivity
import com.example.userrongsok.ui.mainmenu.MainMenuActivity
import com.example.userrongsok.util.FirebaseHelper
import org.koin.android.ext.android.inject

class SplashActivity : AppCompatActivity() {

    private val firebaseHelper by inject<FirebaseHelper>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (firebaseHelper.firebaseLogin()){
            Handler().postDelayed({
                startActivity(Intent(this, MainMenuActivity::class.java))
                finish()
            },3000)
        }else{
            Handler().postDelayed({
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            },3000)
        }
    }
}