package com.example.userrongsok.ui.mainmenu.pengepul

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Toko
import com.example.userrongsok.model.Transaksi
import com.example.userrongsok.repo.firebase.FirebasePengepulRepo
import com.example.userrongsok.repo.firebase.FirebaseTransaksiRepo
import com.example.userrongsok.util.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class DataTransaksiViewModel(
    private val auth: FirebaseAuth,
    private val repoTransaksi: FirebaseTransaksiRepo,
    private val repoPengepul : FirebasePengepulRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {

    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _dataTransaksi = MutableLiveData<MutableList<Transaksi>>()
    val dataTransaksi: LiveData<MutableList<Transaksi>> get() = _dataTransaksi

    private val _dataPengepul = MutableLiveData<MutableList<Toko>>()
    val dataPengepul: LiveData<MutableList<Toko>> get() = _dataPengepul

    fun getDataTransaksi() {
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result =
                    repoTransaksi.getOneTransaksi(auth.currentUser?.uid.toString())) {
                    is RepositoryResult.Success -> {
                        _dataTransaksi.postValue(result.data)
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        _errorLiveData.postValue("")
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                }
            }
        }
    }
    fun getListPengepul(){
        CoroutineScope(dispatchers.io()).launch {
            try {
                when(val result = repoPengepul.getDataToko()){
                    is RepositoryResult.Success->{
                        _dataPengepul.postValue(result.data)
                    }
                    is RepositoryResult.Error-> withContext(dispatchers.main()){
                        _errorLiveData.postValue("Gagal Memuat Data Pengepul")
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()){
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            }catch (e: Exception){
                withContext(dispatchers.main()){
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                }
            }
        }
    }

    class Factory(
        private val auth: FirebaseAuth,
        private val repoTransaksi: FirebaseTransaksiRepo,
        private val repoPengepul: FirebasePengepulRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return DataTransaksiViewModel(auth, repoTransaksi,repoPengepul, dispatchers) as T
        }
    }
}