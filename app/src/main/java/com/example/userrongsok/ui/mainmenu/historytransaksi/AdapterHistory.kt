package com.example.userrongsok.ui.mainmenu.historytransaksi

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.userrongsok.R
import com.example.userrongsok.model.Transaksi
import kotlinx.android.synthetic.main.item_history.view.*

class AdapterHistory(
    private var context: Context,
    private val data: MutableList<Transaksi> = mutableListOf()
) : RecyclerView.Adapter<AdapterHistory.listHistory>() {
    inner class listHistory(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(transaksi: Transaksi){
            itemView.tv_nama_tokoTransaksi.text = transaksi.nama_toko
            itemView.tv_alamatTransaksi.text = transaksi.address
            itemView.tv_harga_transaksi.text = transaksi.jumlah_harga.toString()
            itemView.tv_statusTransaksi.text = transaksi.status
        }
    }
    fun setData(data: MutableList<Transaksi>){
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterHistory.listHistory {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        return listHistory(view)
    }

    override fun onBindViewHolder(holder: AdapterHistory.listHistory, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }
}