package com.example.userrongsok.ui.chat

class Chat(val id: String, val text: String, val fromId: String, val toId: String, val timestamp: Long){
    constructor(): this("","","","",-1)
}