package com.example.userrongsok.ui.regis

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.R
import com.example.userrongsok.model.Users
import com.example.userrongsok.ui.login.LoginActivity
import com.example.userrongsok.util.FirebaseHelper
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_regis.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import kotlin.coroutines.CoroutineContext

class RegisActivity : AppCompatActivity() {
    private val firebaseHelper by inject<FirebaseHelper>()
    private lateinit var viewModel: RegisterViewModel
    private val factory: RegisterViewModel.Factory by inject()
    lateinit var tokenFcm: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_regis)

        viewModel = ViewModelProvider(this, factory).get(RegisterViewModel::class.java)

        viewModel.errorLiveData.observe(this, onError())
        viewModel.successLiveData.observe(this, onSuccess())
        //fungsi daftar

        btn_regis.setOnClickListener {
            showloading(true)
            registerUser()
        }

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("Token","Fetching FCM registration failed", task.exception)
                return@addOnCompleteListener
            }
            //getToken
            tokenFcm = task.result?:""
            Log.d("Token", tokenFcm)
        }
    }

    fun registerUser() {
        var name = et_daftar_nama.text.toString().trim { it <= ' ' }
        var email = et_daftar_email_user.text.toString().trim { it <= ' ' }
        var pass = et_password_daftar_user.text.toString().trim { it <= ' ' }
        var phone = et_daftar_noTelp.text.toString().trim { it <= ' ' }
        when {
            name.isEmpty() -> {
                showError("Field Nama Tidak Boleh Kosong")
                showloading(false)
            }
            email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                showError("Email Kosong atau Tidak Valid")
                showloading(false)
            }
            pass.isEmpty() || pass.length < 8 -> {
                showError("Password Tidak Boleh Kosong Atau Kurang Dari 8 Karakter")
                showloading(false)
            }
            phone.isEmpty() || phone.length < 11 -> {
                showError("Nomor Handphone Tidak Valid")
                showloading(false)
            }
            else -> {
                viewModel.registerFirebase(name, email, pass, phone, tokenFcm)

                Log.d(
                    RegisActivity::class.java.simpleName,
                    "Informasi Pendaftar : Nama($name), Email($email), Nomor Handphone ($phone)"
                )
            }
        }
//        viewModel.saveToDatabase(name, tokenFcm)
    }

    fun onError() = Observer<String> {
        showError(it)
        showloading(false)
    }

    fun onSuccess() = Observer<String> {
        runOnUiThread {
            showloading(false)
            firebaseHelper.firebaseSignOut()
            startActivity(Intent(this, LoginActivity::class.java)).apply {
                Intent.FLAG_ACTIVITY_CLEAR_TOP
                Intent.FLAG_ACTIVITY_CLEAR_TASK
                Intent.FLAG_ACTIVITY_NEW_TASK
            }
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            finish()
        }
    }

    fun showloading(state: Boolean) {
        if (state) {
            pb_regis.visibility = View.VISIBLE
        } else {
            pb_regis.visibility = View.GONE
        }
    }

    fun showError(msg: String) {
        val snackbar = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.errorMsg))
        snackbar.setTextColor(ContextCompat.getColor(this, R.color.teksPutih))
        snackbar.show()
    }
}