package com.example.userrongsok.ui.mainmenu.transaksi.detailtransaksinot

import android.content.Intent
import android.location.Geocoder
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.net.toUri
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.userrongsok.R
import com.example.userrongsok.model.*
import com.example.userrongsok.ui.chat.ActivityChat
import com.example.userrongsok.ui.mainmenu.MainMenuActivity
import kotlinx.android.synthetic.main.activity_detail_transaksi_notification.*
import org.koin.android.ext.android.inject
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DetailTransaksiNotification : AppCompatActivity() {
    private lateinit var adapterDetail: AdapterDetailTransaksiNotification

    private lateinit var viewModel: DetailTransaksiNotVM
    private val factory by inject<DetailTransaksiNotVM.Factory>()
    private lateinit var uriPhoto: Uri
    private val dataIntent by lazy {
        intent.getParcelableExtra<Transaksi>("dataTransaksi")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_transaksi_notification)
        adapterDetail = AdapterDetailTransaksiNotification(this, arrayListOf())
        viewModel = ViewModelProvider(this, factory).get(DetailTransaksiNotVM::class.java)
        viewModel.errorLiveData.observe(this, onError())
        viewModel.dataTransaksi.observe(this, observeGetDetail())
        viewModel.photoLiveData.observe(this, onSuccessDownloadPhoto())

        container_detailTransaksiNotification.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        container_detailTransaksiNotification.adapter = adapterDetail

        viewModel.getDataDetail(dataIntent.id_pengepul)
        getDataIntent()

        btn_cancelRongsok.setOnClickListener {
            viewModel.batalTransaksi(dataIntent.id_pengepul)
            startActivity(Intent(this, MainMenuActivity::class.java)).also {
                Toast.makeText(this, "Transaksi Dibatalkan", Toast.LENGTH_LONG).show()
                Intent.FLAG_ACTIVITY_CLEAR_TOP
                Intent.FLAG_ACTIVITY_NEW_TASK
            }
        }
    }
//
    private fun onSuccessDownloadPhoto() = Observer<Uri> {uri->
        this.uriPhoto = uri
        Glide.with(this)
            .load(uri)
            .apply(RequestOptions().centerCrop())
            .into(iv_foto_rongsok_transaksiNotification)
    }

    private fun onError() = Observer<String> {
        Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
    }

    private fun observeGetDetail() = Observer<MutableList<RongsokTransaksiAdapter>> {list_rongsok->
        runOnUiThread {
            if (list_rongsok.isEmpty()){
                Toast.makeText(this,"Tidak ada data rongsok", Toast.LENGTH_SHORT).show()
                Log.d("List Data", "result: $list_rongsok")
            }else{
                adapterDetail.setData(list_rongsok)
                if (dataIntent.isProccess == true){
                    btn_chat.visibility = View.VISIBLE
                    btn_cancelRongsok.visibility = View.INVISIBLE
                    btn_chat.setOnClickListener {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=${dataIntent.noTelpToko}"))
                        startActivity(intent)
                    }
                }
            }
        }
    }

    fun getDataIntent() {
//        var geocoder = Geocoder(this, Locale.getDefault())
        tv_catatanTransaksi.text = dataIntent.catatan
        tv_status_detailTransaksi.text = dataIntent.status
        tv_total_hargaDetail.text = dataIntent.jumlah_harga.toString()
        tv_alamatPelangganNotif.text = dataIntent.address
//        var data = geocoder.getFromLocation(dataIntent.latitude,dataIntent.longitude,1)
//        address = data.get(0).getAddressLine(0)
//        tv_alamatPelangganNotif.text = address
        viewModel.downloadPhoto(dataIntent.nama_toko)
    }
}