package com.example.userrongsok.ui.chat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.userrongsok.R
import com.example.userrongsok.model.Notifications
import com.example.userrongsok.model.Transaksi
import com.example.userrongsok.util.ConstantsApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.chat_row_from.view.*
import kotlinx.android.synthetic.main.chat_row_to.view.*

const val TAG = "ChatLog"

class ActivityChat : AppCompatActivity() {
//    var auth = FirebaseAuth.getInstance()
    val adapter = GroupAdapter<ViewHolder>()

    private val dataIntentTrans by lazy{
        intent.getParcelableExtra<Transaksi>("dataChat")
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        container_chat.adapter = adapter
        listenMessage()

        tv_unamePengepul.text = dataIntentTrans.nama_toko

        btn_sendChat.setOnClickListener {
            sendMessages()
            Log.d(TAG, "Mengirim Pesan")
            et_chat.text.clear()
        }

        Log.d("data intent","result: $dataIntentTrans")
    }

    private fun sendMessages() {

        val text = et_chat.text.toString()

//        val fromId = auth.currentUser?.uid!!  //id pelanggan
        val fromId = dataIntentTrans.id_user
        val toId= dataIntentTrans.id_pengepul //idpengepul
        Log.d("id","id Pengepul ${dataIntentTrans.id_pengepul}")

      if(fromId == null) return

        val reference = FirebaseDatabase.getInstance().getReference("/${ConstantsApp.CHAT}").push()

        val chatMsg = Chat(reference.key!!,text, fromId, toId, System.currentTimeMillis()/1000)
        reference.setValue(chatMsg)
            .addOnSuccessListener {
                Log.d(TAG,"Simpan Pesan Chat: ${reference.key}")
            }
    }

    private fun listenMessage() {
        val ref = FirebaseDatabase.getInstance().getReference("/${ConstantsApp.CHAT}")
        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val chatMessage = snapshot.getValue(Chat::class.java)
                if (chatMessage != null) {
                    Log.d(TAG, chatMessage.text)
                    if (chatMessage.fromId == dataIntentTrans.id_user){
                        adapter.add(ChatToItem(chatMessage.text))
                    }else {
                        adapter.add(ChatFromItem(chatMessage.text))
                    }
                }
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onChildRemoved(snapshot: DataSnapshot) {

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }
}
class ChatFromItem(val text: String): Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.tv_chat_from.text = text
    }

    override fun getLayout(): Int {
        return  R.layout.chat_row_from
    }
}

class ChatToItem(val text: String): Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.tv_chat_to.text = text
    }

    override fun getLayout(): Int {
        return R.layout.chat_row_to
    }

}