package com.example.userrongsok.ui.mainmenu.transaksi.detailtransaksinot

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.RongsokTransaksi
import com.example.userrongsok.model.RongsokTransaksiAdapter
import com.example.userrongsok.model.Transaksi
import com.example.userrongsok.repo.firebase.FirebaseTransaksiRepo
import com.example.userrongsok.util.ConstantsApp
import com.example.userrongsok.util.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.lang.Exception

class DetailTransaksiNotVM(
    private val auth: FirebaseAuth,
    private val repoTransaksi: FirebaseTransaksiRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _dataTransaksi = MutableLiveData<MutableList<RongsokTransaksiAdapter>>()
    val dataTransaksi: LiveData<MutableList<RongsokTransaksiAdapter>> get() = _dataTransaksi

    private val _photoLiveData = MutableLiveData<Uri>()
    val photoLiveData: LiveData<Uri> get() = _photoLiveData

    fun downloadPhoto(nama: String) {
        CoroutineScope(dispatchers.io()).launch {
            try {
                val photo =
                    repoTransaksi.downloadFotoTransaksi(auth.currentUser?.uid.toString(), nama)
                _photoLiveData.postValue(photo)
            } catch (e: Exception) {
                _errorLiveData.postValue("Gagal Memuat Foto")
            }
        }
    }

    fun getDataDetail(idPengepul: String) {
        val dataRongsokAdapter = mutableListOf<RongsokTransaksiAdapter>()
        var allDataRongsokTransaksi = mutableListOf<RongsokTransaksi>()
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoTransaksi.getRongsokDetailTransaksi(idPengepul)) {
                    is RepositoryResult.Success -> {
                        allDataRongsokTransaksi = result.data
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        _errorLiveData.postValue("Gagal Memuat Data Transaksi")
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
                allDataRongsokTransaksi.forEach {
                    val foto_rongsokUri =
                        repoTransaksi.downloadFotoRongsok(it.id_toko, it.nama_rongsok)
                    val rongsokTransaksiAdapter = RongsokTransaksiAdapter(
                        foto_rongsokUri,
                        it.foto_rongsok,
                        it.nama_rongsok,
                        it.id_user,
                        it.id_toko,
                        it.harga_rongsok,
                        it.quantity
                    )
                    dataRongsokAdapter.add(rongsokTransaksiAdapter)
                }
                CoroutineScope(dispatchers.main()).launch {
                    _dataTransaksi.value = dataRongsokAdapter
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                }
            }
        }
    }

    fun batalTransaksi(id_pengepul: String) = CoroutineScope(dispatchers.io()).launch {
        val docRef = Firebase.firestore.collection(ConstantsApp.TRANSAKSI)
        val dataFirestore = docRef.whereEqualTo("id_user", auth.currentUser?.uid.toString())
            .whereEqualTo("id_pengepul", id_pengepul).get().await()
        if (dataFirestore.documents.isNotEmpty()){
            for (document in dataFirestore){
                try {
                    docRef.document(document.id).update("cancel", true)
                    docRef.document(document.id).update("done", true)
                    docRef.document(document.id).update("rated", true)
                    docRef.document(document.id).update("status", "Transaksi Dibatalkan")
                }catch (e: Exception){
                    withContext(dispatchers.main()){
                        _errorLiveData.postValue("Proses Gagal")
                        Log.d("Ubah Status","Proses Gagal")
                        Log.e("Ubah Status","Proses Gagal")
                    }
                }
            }
        }else{
            withContext(dispatchers.main()){
                _errorLiveData.postValue("Data Kosong")
                Log.d("Get Data","Data Kosong")
                Log.e("Get Data","Data Kosong")
            }
        }
    }

    class Factory(
        private val auth: FirebaseAuth,
        private val repoTransaksi: FirebaseTransaksiRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return DetailTransaksiNotVM(auth, repoTransaksi, dispatchers) as T
        }
    }
}