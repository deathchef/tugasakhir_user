package com.example.userrongsok.ui.mainmenu.transaksi

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.net.toUri
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.userrongsok.R
import com.example.userrongsok.model.Toko
import com.example.userrongsok.model.Transaksi
import com.example.userrongsok.ui.mainmenu.transaksi.detailtransaksinot.DetailTransaksiNotification
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.bottomsheet_rating.*
import kotlinx.android.synthetic.main.fragment_transaksi.*
import org.koin.android.ext.android.inject

class TransaksiFragment : Fragment() {

    private lateinit var viewModel: TransaksiViewModel
    private val factory by inject<TransaksiViewModel.Factory>()
    private lateinit var bottomsheet: BottomSheetBehavior<*>

    private lateinit var adapter: AdapterTransaksi
    lateinit var status: String
    lateinit var toko: Toko



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaksi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        status = ""
        adapter = AdapterTransaksi(requireContext())
        viewModel = ViewModelProvider(this, factory).get(TransaksiViewModel::class.java)

        viewModel.errorLiveData.observe(viewLifecycleOwner, onError())
        viewModel.listDataTransaksi.observe(viewLifecycleOwner, observeGetDataTransaksi())
        viewModel.dataToko.observe(viewLifecycleOwner, Observer <Toko>{
            this.toko = it
        })

        bottomsheet = BottomSheetBehavior.from(bottomsheet_rating)
        bottomsheet.state = BottomSheetBehavior.STATE_HIDDEN

        container_transaksi.layoutManager = LinearLayoutManager(
            this@TransaksiFragment.requireContext(),
            LinearLayoutManager.VERTICAL,
            false
        )
        container_transaksi.adapter = adapter
        showLoading(true)
        viewModel.getListTransaksi()

        adapter.setOnClickListener(object : AdapterTransaksi.OnClickListenerCallback {
            override fun onClickListenerCallback(transaksi: Transaksi) {
                sendDataTransaksi(transaksi)
            }
        })
    }

    fun sendDataTransaksi(transaksi: Transaksi) {
        startActivity(
            Intent(
                requireContext(),
                DetailTransaksiNotification::class.java
            ).putExtra("dataTransaksi", transaksi)
        )
    }

    fun observeGetDataTransaksi() = Observer<MutableList<Transaksi>> { list_transaksi ->
        activity?.runOnUiThread {

            var done: Boolean = false
            var rated: Boolean = false
            var id_pengepul =""
            var ratingUser: Double =0.0
            var pengunjungToko: Double = 0.0

            list_transaksi.forEach {
                done = it.isDone
                rated = it.isRated
                id_pengepul = it.id_pengepul
            }

            viewModel.getToko(id_pengepul)

            if (list_transaksi.isEmpty()) {
                container_transaksi.visibility = View.GONE
                iv_noTrans.visibility = View.VISIBLE
                tv_pertama.visibility = View.VISIBLE
                tv_kedua.visibility = View.VISIBLE
                showLoading(false)
            } else {
                if (rated == false) {

                    showLoading(false)
                    if (done) {
                        if (bottomsheet.state == BottomSheetBehavior.STATE_COLLAPSED || bottomsheet.state == BottomSheetBehavior.STATE_HIDDEN) {
                            bottomsheet.state = BottomSheetBehavior.STATE_EXPANDED

                            rating_bar.rating = 0.0f
                            rating_bar.stepSize = 1.0f

                            rating_bar.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
                                ratingUser = this.toko.rating + rating
                                pengunjungToko = this.toko.pengunjung + 1
                            }
                            btn_rating.setOnClickListener {
                                viewModel.updateRatingToko(id_pengepul, ratingUser, pengunjungToko )
                                viewModel.changeStatusTransaksi(id_pengepul)
                                bottomsheet.state = BottomSheetBehavior.STATE_HIDDEN
                                container_transaksi.visibility = View.GONE
                                iv_noTrans.visibility = View.VISIBLE
                                tv_pertama.visibility = View.VISIBLE
                                tv_kedua.visibility = View.VISIBLE
                                showLoading(false)
                            }
                        } else {
                            bottomsheet.state = BottomSheetBehavior.STATE_HIDDEN
                        }
                    } else {
                        adapter.setData(list_transaksi)
                        container_transaksi.visibility = View.VISIBLE
                        showLoading(false)
                        list_transaksi.forEach {
                            status = it.isDone.toString()
                        }
                    }
                } else {
                    container_transaksi.visibility = View.GONE
                    iv_noTrans.visibility = View.VISIBLE
                    tv_pertama.visibility = View.VISIBLE
                    tv_kedua.visibility = View.VISIBLE
                    showLoading(false)
                }
            }
        }
    }

    fun onError() = Observer<String> {
        activity?.runOnUiThread {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }

    fun showLoading(state: Boolean) {
        if (state) {
            pb_transaksi.visibility = View.VISIBLE
        } else
            pb_transaksi.visibility = View.GONE
    }
}