package com.example.userrongsok.ui.mainmenu.transaksi

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Toko
import com.example.userrongsok.model.Transaksi
import com.example.userrongsok.repo.firebase.FirebaseTransaksiRepo
import com.example.userrongsok.util.ConstantsApp
import com.example.userrongsok.util.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.lang.Exception

class TransaksiViewModel(
    private val auth: FirebaseAuth,
    private val repoTransaksi: FirebaseTransaksiRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _successLiveData = MutableLiveData<String>()
    val successLiveData: LiveData<String> get() = _successLiveData

    private val _listDataTransaksi = MutableLiveData<MutableList<Transaksi>>()
    val listDataTransaksi: LiveData<MutableList<Transaksi>> get() = _listDataTransaksi

    private val _dataToko = MutableLiveData<Toko>()
    val dataToko: LiveData<Toko> get() = _dataToko

    fun getListTransaksi() {
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result =
                    repoTransaksi.getDataTransaksi(auth.currentUser?.uid.toString())) {
                    is RepositoryResult.Success -> {
                        _listDataTransaksi.postValue(result.data)
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        _errorLiveData.postValue("Gagal Memuat data Transaksi")
                        Log.d("Error get Transaksi","Gagal Memuat Data Transaksi")
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        result.exception?.message.let {
                            _errorLiveData.postValue("")
                            Log.d("Cancel Get","Memut dibatalkan")
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    e.message?.let {
                        _errorLiveData.postValue("")
                        Log.d("Exception","Exception Data Transaksi")
                    }
                }
            }
        }
    }

    fun getToko(id_pengepul: String) {
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoTransaksi.getToko(id_pengepul)) {
                    is RepositoryResult.Success -> {
                        _dataToko.postValue(result.data)
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        _errorLiveData.postValue("Gagal Get Data Toko")
                        Log.d("Error","Get gagal get Data")
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        result.exception?.message.let {
                            _errorLiveData.postValue("")
                            Log.d("Cancel","Cancel get Data")
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    e.message?.let {
                        Log.d("exception","Exception Data Toko")
                    }
                }
            }
        }
    }

    fun updateRatingToko(id_pengepul: String, rating: Double, pengunjung: Double) {
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoTransaksi.addRating(id_pengepul, rating, pengunjung)) {
                    is RepositoryResult.Success -> {
                        _successLiveData.postValue("Sukses Memberi Rating")
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        _errorLiveData.postValue("Rating Toko Gagal")
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                }
            }
        }
    }

    fun changeStatusTransaksi(id_pengepul: String) = CoroutineScope(dispatchers.io()).launch {
        val docRef = Firebase.firestore.collection(ConstantsApp.TRANSAKSI)
        val dataFirestore = docRef.whereEqualTo("id_user", auth.currentUser?.uid.toString())
            .whereEqualTo("id_pengepul", id_pengepul).get().await()
        if (dataFirestore.documents.isNotEmpty()) {
            for (document in dataFirestore) {
                try {
                    docRef.document(document.id).update("rated", true)
                } catch (e: Exception) {
                    withContext(dispatchers.main()) {
                        _errorLiveData.postValue("Proses Gagal")
                        Log.d("Change State", "result: Gagal")
                    }
                }
            }
        } else {
            withContext(dispatchers.main()) {
                _errorLiveData.postValue("Proses Gagal")
                Log.d("Data Query", "Data Kosong")
            }
        }
    }

    class Factory(
        private val auth: FirebaseAuth,
        private val repoTransaksi: FirebaseTransaksiRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return TransaksiViewModel(auth, repoTransaksi, dispatchers) as T
        }
    }
}