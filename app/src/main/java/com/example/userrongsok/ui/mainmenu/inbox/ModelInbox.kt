package com.example.userrongsok.ui.mainmenu.inbox

import com.google.firebase.Timestamp

data class ModelInbox (
    var judul: String = "",
    var tanggal: Timestamp = Timestamp.now(),
    var pesan: String =""
)