package com.example.userrongsok.ui.notification

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.Notifications
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.repo.firebase.FirebaseNotificationRepo
import com.example.userrongsok.util.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Query
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class NotificationViewModel(
    private val auth: FirebaseAuth,
    private val repoNotif: FirebaseNotificationRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _successLiveData = MutableLiveData<String>()
    val successLiveData: LiveData<String> get() = _successLiveData

    private val _notifLiveData = MutableLiveData<MutableList<Notifications>>()
    val notifLiveData: LiveData<MutableList<Notifications>> get() = _notifLiveData

    //    fun getDataNotification() {
//        CoroutineScope(dispatchers.io()).launch {
//            try {
//                when (val result = repoNotif.getNotificationData(auth.currentUser?.uid!!)) {
//                    is RepositoryResult.Success -> {
//                        _notifLiveData.postValue(result.data)
//                        Log.d("Data Notif", "result: ${result.data}")
//                    }
//                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
//                        _errorLiveData.postValue("Gagal Mengambil Data Notification")
//                    }
//                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
//                        result.exception?.message.let {
//                            _errorLiveData.postValue(it)
//                        }
//                    }
//                }
//            } catch (e: Exception) {
//                withContext(dispatchers.main()) {
//                    e.message?.let {
//                        _errorLiveData.postValue(it)
//                    }
//                }
//            }
//        }
//    }
    val dataNotif = MutableLiveData<Query>()
    private val compositeDisposable = CompositeDisposable()
    fun getDataNotif() {
        compositeDisposable.add(repoNotif.getDataNotif(auth.currentUser?.uid!!).subscribe({
            dataNotif.postValue(it)
        },{

        }))
    }

    class Factory(
        private val auth: FirebaseAuth,
        private val repoNotif: FirebaseNotificationRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return NotificationViewModel(auth, repoNotif, dispatchers) as T
        }
    }
}