package com.example.userrongsok.repo

import com.example.userrongsok.model.BodyPayloadNotif
import com.example.userrongsok.model.Notifications
import com.example.userrongsok.model.RepositoryResult
import com.google.firebase.firestore.Query
import io.reactivex.Single
import kotlinx.coroutines.Job

interface NotificationRepo {
    suspend fun addDataNotif(
        notificationData: BodyPayloadNotif,
        pengepulId: String
    ): RepositoryResult<String>
    suspend fun getNotificationData(id_user: String): RepositoryResult<MutableList<Notifications>>
    fun getDataNotif(id_user: String): Single<Query>
}