package com.example.userrongsok.repo

import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Users

interface UserRepo {
    suspend fun addUserData(user: Users): RepositoryResult<String>
//    suspend fun updateUsername(userMap: Map<String, String>): RepositoryResult<String>
    suspend fun updateProfil(nama:String, nomor_handphone:String): RepositoryResult<String>
    suspend fun getDataUser(userId: String): RepositoryResult<Users>
    suspend fun getDataUserById(userId: String): RepositoryResult<Users>
}