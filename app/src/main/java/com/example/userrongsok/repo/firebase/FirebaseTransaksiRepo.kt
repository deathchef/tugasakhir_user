package com.example.userrongsok.repo.firebase

import android.net.Uri
import android.util.Log
import com.example.userrongsok.model.*
import com.example.userrongsok.repo.TransaksiRepo
import com.example.userrongsok.util.ConstantsApp
import com.example.userrongsok.util.FirebaseTaskExtention.awaits
import com.example.userrongsok.util.firestore
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.coroutines.tasks.await
import java.util.ArrayList

class FirebaseTransaksiRepo(private val mStorageRef: StorageReference) : TransaksiRepo {
    var auth = FirebaseAuth.getInstance()
    var uid = auth.currentUser?.uid!!

    override suspend fun downloadFotoTransaksi(uid: String, nama: String): Uri {
        val ref = mStorageRef.child("transaksi/$uid$nama.jpg")
        return ref.downloadUrl.await()
    }

    override suspend fun addTransaksi(transaksi: Transaksi): RepositoryResult<String> {
        val firestore = Firebase.firestore.collection(ConstantsApp.TRANSAKSI)
        return when (val query = firestore.add(transaksi).awaits()) {
            is RepositoryResult.Success -> RepositoryResult.Success("Pengajuan Rongsok Sukses")
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun addFotoTransaksi(
        uid: String,
        nama: String,
        foto_transaksi: Uri,
        onResult: (Uri) -> Unit
    ) {
        val ref = mStorageRef.child("transaksi/$uid$nama.jpg")
        ref.putFile(foto_transaksi)
            .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> {
                return@Continuation ref.downloadUrl
            }).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    if (downloadUri != null) {
                        onResult(downloadUri)
                    }
                }
            }
    }

    override suspend fun getDataTransaksi(id: String): RepositoryResult<MutableList<Transaksi>> {
        val firestore =
            Firebase.firestore.collection(ConstantsApp.TRANSAKSI).whereEqualTo("id_user", id)
                .whereEqualTo("cancel", false).whereEqualTo("rated", false)
        var list_transaksi = mutableListOf<Transaksi>()
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(list_transaksi)
                    Log.e("Get Data Transaksi", "Gagal memuat data transaksi")
                } else {
                    query.data.documents.forEach { doc ->
                        val data = doc.toObject<Transaksi>()
                        data?.let {
                            list_transaksi.add(data)
                        }
                        Log.d("Get Data Transaksi", data.toString())
                    }
                }
                RepositoryResult.Success(list_transaksi)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun getOneTransaksi(id: String): RepositoryResult<MutableList<Transaksi>> {
        val firestore =
            Firebase.firestore.collection(ConstantsApp.TRANSAKSI).whereEqualTo("id_user", id)
                .whereEqualTo("rated", false)
        var dataTransaksi = mutableListOf<Transaksi>()
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(dataTransaksi)
                    Log.e("Get Data Transaksi", "Tidak Ada Data Transaksi")
                } else {
                    query.data.documents.forEach { doc ->
                        val data = doc.toObject<Transaksi>()
                        data?.let {
                            dataTransaksi.add(data)
                        }
                    }
                }
                RepositoryResult.Success(dataTransaksi)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun getAllTransaksi(id: String): RepositoryResult<MutableList<Transaksi>> {
        val docRef =
            Firebase.firestore.collection(ConstantsApp.TRANSAKSI).whereEqualTo("id_user", id)
                .whereEqualTo("done", true)
        var dataTransaksi = mutableListOf<Transaksi>()
        return when (val query = docRef.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(dataTransaksi)
                    Log.e("Get All Transaksi", "Tidak ada data transaksi")
                } else {
                    query.data.documents.forEach { doc ->
                        val data = doc.toObject<Transaksi>()
                        data?.let {
                            dataTransaksi.add(data)
                        }
                    }
                }
                RepositoryResult.Success(dataTransaksi)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }


    override suspend fun addRating(
        id_pengepul: String,
        ratingUser: Double,
        pengunjung: Double
    ): RepositoryResult<String> {
        val docRef = Firebase.firestore.collection(ConstantsApp.TOKO).document(id_pengepul)
        val toko = mapOf("rating" to ratingUser, "pengunjung" to pengunjung)

        return when (val query = docRef.set(toko, SetOptions.merge()).awaits()) {
            is RepositoryResult.Success -> RepositoryResult.Success("Rating Toko Updated")
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun getToko(id_pengepul: String): RepositoryResult<Toko> {
        val docRef = Firebase.firestore.collection(ConstantsApp.TOKO).document(id_pengepul)

        return when (val query = docRef.get().awaits()) {
            is RepositoryResult.Success -> {
                RepositoryResult.Success(query.data.toObject<Toko>()) as RepositoryResult<Toko>
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun getRongsokDetailTransaksi(
        idPengepul: String
    ): RepositoryResult<MutableList<RongsokTransaksi>> {
        val firestore =
            Firebase.firestore.collection(ConstantsApp.TRANSAKSI).document(uid)
                .collection(ConstantsApp.RONGSOK_TRANSAKSI)
                .whereEqualTo("id_toko", idPengepul)
        var listRongsok = mutableListOf<RongsokTransaksi>()
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(listRongsok)
                } else {
                    query.data.documents.forEach { doc ->
                        val data = doc.toObject<RongsokTransaksi>()
                        data?.let {
                            listRongsok.add(data)
                        }
                        Log.d("Get Data Detail", "Result: ${data.toString()}")
                    }
                }
                RepositoryResult.Success(listRongsok)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun addRongsokTransaksi(
        rongsok: RongsokTransaksi,
        id_user: String
    ): RepositoryResult<ArrayList<RongsokTransaksi>> {
        var listRongsok = arrayListOf(rongsok)
        val firestore = Firebase.firestore.collection(ConstantsApp.TRANSAKSI).document(id_user)
            .collection(ConstantsApp.RONGSOK_TRANSAKSI)

        return when (val query = firestore.add(rongsok).awaits()) {
            is RepositoryResult.Success -> {
                RepositoryResult.Success(listRongsok)
            }
            is RepositoryResult.Error -> {
                RepositoryResult.Error(query.exception)
            }
            is RepositoryResult.Canceled -> {
                RepositoryResult.Canceled(query.exception)
            }
        }
    }

    override suspend fun downloadFotoRongsok(id: String, nama_rongsok: String): Uri {
        val ref = mStorageRef.child("/rongsok/$id$nama_rongsok.jpg")
        return ref.downloadUrl.await()
    }
}