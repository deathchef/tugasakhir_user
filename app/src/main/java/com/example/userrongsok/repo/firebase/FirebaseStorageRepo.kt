package com.example.userrongsok.repo.firebase

import android.net.Uri
import com.example.userrongsok.repo.StorageRepo
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.tasks.await

class FirebaseStorageRepo(private val mStorageRef: StorageReference): StorageRepo {

    override suspend fun downloadFotoRongsok(id: String, nama_barang: String): Uri {
        val ref = mStorageRef.child("rongsok/$id$nama_barang.jpg")
        return ref.downloadUrl.await()
    }

    override suspend fun downloadFotoToko(id: String, nama_toko: String): Uri {
        val ref = mStorageRef.child("toko/$id$nama_toko.jpg")
        return ref.downloadUrl.await()
    }
}