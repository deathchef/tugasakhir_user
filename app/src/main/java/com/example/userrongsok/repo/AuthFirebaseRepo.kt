package com.example.userrongsok.repo

import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Users

interface AuthFirebaseRepo {
    suspend fun login (email: String, password: String): RepositoryResult<String>
    suspend fun register(nama: String, email: String, password: String, noHp: String, tokenFCM: String): RepositoryResult<String>
//    suspend fun saveDatabase (nama: String, tokenFCM: String): RepositoryResult<String>
}