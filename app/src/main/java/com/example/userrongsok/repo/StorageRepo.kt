package com.example.userrongsok.repo

import android.net.Uri

interface StorageRepo {
    suspend fun downloadFotoRongsok(id: String, nama_barang: String): Uri
    suspend fun downloadFotoToko(id: String, nama_toko: String): Uri
}