package com.example.userrongsok.repo.firebase

import android.util.Log
import com.example.userrongsok.model.BodyPayloadNotif
import com.example.userrongsok.model.Notifications
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.repo.NotificationRepo
import com.example.userrongsok.util.ConstantsApp
import com.example.userrongsok.util.FirebaseTaskExtention.awaits
import com.google.firebase.firestore.core.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import io.reactivex.Single

class FirebaseNotificationRepo : NotificationRepo {

    override suspend fun addDataNotif(
        notificationData: BodyPayloadNotif,
        pengepulId: String
    ): RepositoryResult<String> {
        val firestore =
            Firebase.firestore.collection(ConstantsApp.NOTIFICATION).document(pengepulId)
                .collection("data")
        return when (val query = firestore.add(notificationData).awaits()) {
            is RepositoryResult.Success -> {
                RepositoryResult.Success("Success")
            }
            is RepositoryResult.Error -> {
                RepositoryResult.Error(query.exception)
            }
            is RepositoryResult.Canceled -> {
                query.exception.let {
                    RepositoryResult.Canceled(it)
                }
            }
        }
    }

    override suspend fun getNotificationData(id_user: String): RepositoryResult<MutableList<Notifications>> {
        val firestore = Firebase.firestore.collection(ConstantsApp.NOTIFICATION).document(id_user)
            .collection("data")
            .orderBy("date", com.google.firebase.firestore.Query.Direction.DESCENDING)

        val listNotif = mutableListOf<Notifications>()

        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(listNotif)
                    Log.d("Get Data notif", "Gagal Mengambil Data")
                } else {
                    query.data.documents.forEach { doc ->
                        val data = doc.toObject<Notifications>()
                        data?.let {
                            listNotif.add(data)
                        }
                    }
                }
                RepositoryResult.Success(listNotif)
            }
            is RepositoryResult.Error -> {
                Log.e("Error", "result: ${query.exception.message}")
                RepositoryResult.Error(query.exception)
            }
            is RepositoryResult.Canceled -> {
                RepositoryResult.Canceled(query.exception)
            }
        }
    }

    override fun getDataNotif(id_user: String): Single<com.google.firebase.firestore.Query> =
        Single.create {
            val firestore =
                Firebase.firestore.collection(ConstantsApp.NOTIFICATION).document(id_user)
                    .collection("data")
                    .orderBy("date", com.google.firebase.firestore.Query.Direction.DESCENDING)

            it.onSuccess(firestore)
        }
}