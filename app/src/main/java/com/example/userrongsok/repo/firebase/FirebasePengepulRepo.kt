package com.example.userrongsok.repo.firebase

import android.util.Log
import com.example.userrongsok.model.Pengepul
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Rongsok
import com.example.userrongsok.model.Toko
import com.example.userrongsok.repo.PengepulRepo
import com.example.userrongsok.util.ConstantsApp
import com.example.userrongsok.util.FirebaseTaskExtention.awaits
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase

class FirebasePengepulRepo : PengepulRepo {
    override suspend fun getDataToko(): RepositoryResult<MutableList<Toko>> {
        val firestore = Firebase.firestore.collection(ConstantsApp.TOKO)
        var listToko = mutableListOf<Toko>()
        return when(val query = firestore.get().awaits()){
            is RepositoryResult.Success ->{
                if (query.data.isEmpty){
                    RepositoryResult.Success(listToko)
                    Log.w("Get Data Toko", "Data Toko Gagal Dimuat")
                }else{
                    query.data.documents.forEach {doc->
                        val data = doc.toObject<Toko>()
                        data?.let {
                            listToko.add(data)
                        }
                    }
                }
                RepositoryResult.Success(listToko)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled-> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun getDataRongsok(id: String): RepositoryResult<MutableList<Rongsok>> {
        val firestore = Firebase.firestore.collection(ConstantsApp.TOKO).document(id).collection(ConstantsApp.RONGSOK)
        val listRongsok = mutableListOf<Rongsok>()
        return when(val query = firestore.get().awaits()){
            is RepositoryResult.Success->{
                if (query.data.isEmpty){
                    RepositoryResult.Success(listRongsok)
                    Log.w("Get Data Rongsok", "Gagal Memuat Data Rongsok")
                }else{
                    query.data.documents.forEach {doc->
                        val data = doc.toObject<Rongsok>()
                        data?.let {
                            listRongsok.add(data)
                        }
                        Log.d("Get Data Rongsok", data.toString())
                    }
                }
                RepositoryResult.Success(listRongsok)
            }
            is RepositoryResult.Error-> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun getDataPengepul(): RepositoryResult<MutableList<Pengepul>> {
        val firestore = Firebase.firestore.collection(ConstantsApp.PENGEPUL)
        var listPengepul = mutableListOf<Pengepul>()
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(listPengepul)
                } else {
                    query.data.documents.forEach { doc ->
                        val data = doc.toObject(Pengepul::class.java)
                        data?.let {
                            data.id = doc.id
                            listPengepul.add(data)
                        }
                    }
                }
                RepositoryResult.Success(listPengepul)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }
}