package com.example.userrongsok.repo.firebase

import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Users
import com.example.userrongsok.repo.UserRepo
import com.example.userrongsok.util.ConstantsApp
import com.example.userrongsok.util.FirebaseTaskExtention.awaits
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import com.google.firebase.ktx.Firebase

class FirebaseUserRepo(private val auth: FirebaseAuth) : UserRepo {
    override suspend fun addUserData(user: Users): RepositoryResult<String> {
        val firestore = Firebase.firestore
            .document("User: ${auth.currentUser?.uid}")
        return when (val query = firestore.set(user).awaits()) {
            is RepositoryResult.Success -> RepositoryResult.Success("Add User Berhasil")
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> query.exception.let {
                RepositoryResult.Canceled(it)
            }
        }
    }

//    override suspend fun updateUsername(userMap: Map<String, String>): RepositoryResult<String> {
//        val docRef = Firebase.firestore.collection(ConstantsApp.USER)
//            .document(auth.currentUser?.uid.toString())
//        return when (val query = docRef.set(userMap, SetOptions.merge()).awaits()) {
//            is RepositoryResult.Success -> RepositoryResult.Success("Update username User Success")
//            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
//            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
//        }
//    }

    override suspend fun updateProfil(nama: String, nomor_handphone: String): RepositoryResult<String> {
        val docRef = Firebase.firestore.collection(ConstantsApp.USER)
            .document(auth.currentUser?.uid.toString())

        val user = mapOf<String, String>("name" to nama, "phone" to nomor_handphone)
        return when (val query = docRef.set(user, SetOptions.merge()).awaits()) {
            is RepositoryResult.Success -> RepositoryResult.Success("Query Didapat")
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }


    override suspend fun getDataUser(userId: String): RepositoryResult<Users> {
        val firestore = Firebase.firestore.collection(ConstantsApp.USER)
            .whereEqualTo("id", userId)
        var userData = Users()
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(userData)
                } else {
                    val user = query.data.toObjects<Users>()
                    userData = user[0]
                }
                RepositoryResult.Success(userData)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun getDataUserById(userId: String): RepositoryResult<Users> {
        val firestore = Firebase.firestore
            .document("${auth.currentUser?.uid}")
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                lateinit var userData: Users
                if (query.data.exists()) {
                    query.data.toObject<Users>()?.let {
                        userData = it
                    }
                } else {
                    userData = Users(
                        auth.currentUser?.uid!!,
                        auth.currentUser?.displayName!!,
                        auth.currentUser?.email!!,
                        auth.currentUser?.phoneNumber!!,
                        false,
                        "tokenInstance.token"
                    )
                }
                RepositoryResult.Success(userData)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }
}