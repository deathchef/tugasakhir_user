package com.example.userrongsok.repo.firebase

import android.util.Log
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.UserDB
import com.example.userrongsok.model.Users
import com.example.userrongsok.repo.AuthFirebaseRepo
import com.example.userrongsok.services.MyFirebaseMessagingService
import com.example.userrongsok.util.FirebaseTaskExtention.awaits
import com.example.userrongsok.util.firestore
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.SetOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.installations.FirebaseInstallations
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging


class FirebaseAuthRepo(private val auth: FirebaseAuth) : AuthFirebaseRepo {
    override suspend fun login(email: String, password: String): RepositoryResult<String> {
        return when (val query = auth.signInWithEmailAndPassword(email, password).awaits()) {
            is RepositoryResult.Success -> RepositoryResult.Success("Login Berhasil")
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> query.exception.let {
                RepositoryResult.Canceled(it)
            }
        }
    }

    override suspend fun register(
        nama: String,
        email: String,
        password: String,
        noHp: String,
        tokenFCM: String
    ): RepositoryResult<String> {
        when (val query = auth.createUserWithEmailAndPassword(email, password).awaits()) {
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> query.exception.let {
                RepositoryResult.Canceled(it)
            }
        }
        val uid = auth.currentUser?.uid.toString()

        val user = Users(
            id = uid,
            name = nama,
            email = email,
            phone = noHp,
            banned = false,
            tokenNotif = tokenFCM
        )

        val database = Firebase.database
        val ref = database.getReference("/users/$uid")
        return when (val db =
            firestore.collection("users").document(uid).set(user, SetOptions.merge()).awaits()) {
            is RepositoryResult.Success -> {
                auth.currentUser?.let { it ->
                    val profilUpdate = UserProfileChangeRequest.Builder()
                        .setDisplayName(nama)
                        .build()
                    it.updateProfile(profilUpdate)
                }
                ref.setValue(user).addOnSuccessListener {
                    Log.d("Database","succes save database")
                }
                RepositoryResult.Success("Register Berhasil")
            }
            is RepositoryResult.Error -> RepositoryResult.Error(db.exception)
            is RepositoryResult.Canceled -> db.exception.let {
                RepositoryResult.Canceled(it)
            }
        }
        Log.d(FirebaseAuthRepo::class.java.simpleName,"info User: ${user}")
    }

//    override suspend fun saveDatabase(nama: String, tokenFCM: String): RepositoryResult<String> {
//        val database = Firebase.database
//        val ref = database.getReference("/users/${auth.currentUser?.uid!!}")
//        val users = UserDB(
//            id = auth.currentUser?.uid!!,
//            name = nama,
//            tokenFcm = tokenFCM
//        )
//
//        return when (val db = ref.setValue(users).awaits()){
//            is RepositoryResult.Success -> RepositoryResult.Success("Data Tersimpan di Firebase Database")
//            is RepositoryResult.Error -> RepositoryResult.Error(db.exception)
//            is RepositoryResult.Canceled -> db.exception.let {
//                RepositoryResult.Canceled(it)
//            }
//        }
//    }
}