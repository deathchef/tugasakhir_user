package com.example.userrongsok.repo

import com.example.userrongsok.model.Pengepul
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Rongsok
import com.example.userrongsok.model.Toko

interface PengepulRepo {
    suspend fun getDataToko(): RepositoryResult<MutableList<Toko>>
    suspend fun getDataRongsok(id: String): RepositoryResult<MutableList<Rongsok>>
    suspend fun getDataPengepul(): RepositoryResult<MutableList<Pengepul>>
}