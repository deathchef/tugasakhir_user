package com.example.userrongsok.repo.firebase

import android.util.Log
import com.example.userrongsok.admin.jenisrongsok.ModelJenis
import com.example.userrongsok.admin.user.DataUser
import com.example.userrongsok.model.Pengepul
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Users
import com.example.userrongsok.repo.AdminRepo
import com.example.userrongsok.util.ConstantsApp
import com.example.userrongsok.util.FirebaseTaskExtention.awaits
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import com.google.firebase.ktx.Firebase

class FirebaseAdmin() : AdminRepo {
    override suspend fun getDataUser(): RepositoryResult<MutableList<Users>> {
        val firestore = Firebase.firestore.collection(ConstantsApp.USER)
        var listUser = mutableListOf<Users>()
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {

                if (query.data.isEmpty) {
                    RepositoryResult.Success(listUser)
                } else {
                    query.data.documents.forEach { doc ->
                        val data = doc.toObject<Users>()
                        data?.let {
                            listUser.add(data)
                        }
                    }
                }
                RepositoryResult.Success(listUser)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun getDataPengepul(): RepositoryResult<MutableList<Pengepul>> {
        val firestore = Firebase.firestore.collection(ConstantsApp.PENGEPUL)
        val listPengepul = mutableListOf<Pengepul>()
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(listPengepul)
                } else {
                    query.data.documents.forEach { doc ->
                        val data = doc.toObject<Pengepul>()
                        data?.let {
                            listPengepul.add(data)
                            Log.d(FirebaseAdmin::class.java.simpleName, "list Nama : $listPengepul")
                        }
                    }
                }
                RepositoryResult.Success(listPengepul)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }

    }

    override suspend fun addJenisRongsok(jenis: String): RepositoryResult<String> {
        val firestore = Firebase.firestore.collection(ConstantsApp.JENIS_RONGSOK)
        val nama_jenis = ModelJenis(nama_jenis = jenis)
        return when (val query = firestore.add(nama_jenis).awaits()) {
            is RepositoryResult.Success -> RepositoryResult.Success("Berhasil Menambahkan Jenis Rongsok")
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> query.exception.let {
                RepositoryResult.Canceled(it)
            }
        }
    }


    override suspend fun getJenisRongsok(): RepositoryResult<MutableList<ModelJenis>> {
        val firestore = Firebase.firestore.collection(ConstantsApp.JENIS_RONGSOK)
        val listJenis = mutableListOf<ModelJenis>()

        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(listJenis)
                } else {
                    query.data.documents.forEach { doc ->
                        val data = doc.toObject<ModelJenis>()
                        data?.let {
                            listJenis.add(data)
                        }
                    }
                }
                RepositoryResult.Success(listJenis)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }
}