package com.example.userrongsok.repo

import com.example.userrongsok.admin.jenisrongsok.ModelJenis
import com.example.userrongsok.model.Pengepul
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Users

interface AdminRepo {
    suspend fun getDataUser(): RepositoryResult<MutableList<Users>>
    suspend fun getDataPengepul():RepositoryResult<MutableList<Pengepul>>
    suspend fun addJenisRongsok(jenis: String): RepositoryResult<String>
    suspend fun getJenisRongsok(): RepositoryResult<MutableList<ModelJenis>>
}