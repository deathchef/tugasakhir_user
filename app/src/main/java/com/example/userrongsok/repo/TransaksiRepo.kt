package com.example.userrongsok.repo

import android.net.Uri
import com.example.userrongsok.model.*
import java.util.ArrayList

interface TransaksiRepo {
    suspend fun downloadFotoTransaksi(uid: String, nama: String):Uri
    suspend fun addTransaksi(transaksi: Transaksi): RepositoryResult<String>
    suspend fun addFotoTransaksi(uid: String,nama:String, foto_transaksi: Uri,onResult: (Uri)->Unit)
    suspend fun getDataTransaksi(id: String): RepositoryResult<MutableList<Transaksi>>
    suspend fun getRongsokDetailTransaksi(idPengepul: String): RepositoryResult<MutableList<RongsokTransaksi>>
    suspend fun addRongsokTransaksi(rongsok: RongsokTransaksi, id_user: String): RepositoryResult<ArrayList<RongsokTransaksi>>
    suspend fun downloadFotoRongsok(id: String, nama_rongsok: String): Uri
    suspend fun getOneTransaksi(id: String): RepositoryResult<MutableList<Transaksi>>
    suspend fun addRating(id_pengepul: String, rating: Double, pengunjung: Double): RepositoryResult<String>
    suspend fun getToko(id_pengepul: String): RepositoryResult<Toko>
    suspend fun getAllTransaksi(id: String): RepositoryResult<MutableList<Transaksi>>
}