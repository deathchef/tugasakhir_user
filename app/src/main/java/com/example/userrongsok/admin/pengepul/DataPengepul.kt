package com.example.userrongsok.admin.pengepul

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.userrongsok.R
import com.example.userrongsok.model.Pengepul
import kotlinx.android.synthetic.main.dialog_banned_pengguna.view.*
import kotlinx.android.synthetic.main.fragment_data_pengepul.*
import kotlinx.android.synthetic.main.item_list_user.view.*
import org.koin.android.ext.android.inject

class DataPengepul : Fragment() {

    private lateinit var viewModel: PengepulViewModel
    private val factory by inject<PengepulViewModel.Factory>()

    private lateinit var adapter: AdapterPengepul

    private val dialogBannedPengguna by lazy {
        layoutInflater.inflate(R.layout.dialog_banned_pengguna, null, false)
    }
    private lateinit var showDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_data_pengepul, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = AdapterPengepul()
        viewModel = ViewModelProvider(this, factory).get(PengepulViewModel::class.java)

        viewModel.errorLiveData.observe(viewLifecycleOwner, onError())
        viewModel.listPengepul.observe(viewLifecycleOwner, observeGetDataPengepul())
        container_listPengepul.layoutManager = LinearLayoutManager(
            this@DataPengepul.requireContext(),
            LinearLayoutManager.VERTICAL,
            false
        )
        container_listPengepul.adapter = adapter
        showLoading(true)
        viewModel.getListPengepul()

        adapter.setOnClickListener(object : AdapterPengepul.OnClickListenerCallback {
            override fun onClick(pengepul: Pengepul, position: Int) {
                this@DataPengepul.let {
                    if (dialogBannedPengguna.parent != null) {
                        val viewGroup = dialogBannedPengguna.parent as ViewGroup
                        viewGroup.removeView(dialogBannedPengguna)
                    }
                    val dialog = AlertDialog.Builder(requireContext())
                    dialog.setView(dialogBannedPengguna)
                    showDialog = dialog.create()
                    showDialog.show()

                    dialogBannedPengguna.btn_banned_pengguna.setOnClickListener {
                        Toast.makeText(
                            dialogBannedPengguna.context,
                            "Pengguna ini dinonaktifkan",
                            Toast.LENGTH_LONG
                        ).show()
                        showDialog.dismiss()
                    }
                    dialogBannedPengguna.btn_batal_banned.setOnClickListener {
                        showDialog.cancel()
                    }
                }
            }

        })
    }

    private fun observeGetDataPengepul() = Observer<MutableList<Pengepul>> { listPengepul ->
        if (listPengepul.isEmpty()) {
            container_listPengepul.isVisible = false
            showLoading(false)
        } else {
            adapter.setData(listPengepul)
            container_listPengepul.isVisible = true
            showLoading(false)
        }
    }

    fun showLoading(state: Boolean) {
        if (state) {
            pb_listPengepul.visibility = View.VISIBLE
        } else {
            pb_listPengepul.visibility = View.GONE
        }
    }

    private fun onError() = Observer<String> {
        activity?.runOnUiThread {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }
    }

}