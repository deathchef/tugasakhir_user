package com.example.userrongsok.admin.user

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.userrongsok.R
import com.example.userrongsok.model.Users
import kotlinx.android.synthetic.main.dialog_banned_pengguna.view.*
import kotlinx.android.synthetic.main.fragment_data_user.*
import kotlinx.android.synthetic.main.item_list_user.view.*
import org.koin.android.ext.android.inject

class DataUser : Fragment() {
    private lateinit var viewModel: UserViewModel
    private val factory by inject<UserViewModel.Factory>()

    private lateinit var adapterUser: AdapterUser
    private val dialogBannedPengguna by lazy {
        layoutInflater.inflate(R.layout.dialog_banned_pengguna, null, false)
    }
    private lateinit var showDialog: AlertDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_data_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapterUser = AdapterUser()
        viewModel = ViewModelProvider(this, factory).get(UserViewModel::class.java)
        viewModel.errorLiveData.observe(viewLifecycleOwner, onError())
        viewModel.listUser.observe(viewLifecycleOwner, observeGetDataUser())

        container_listUser.layoutManager =
            LinearLayoutManager(this@DataUser.requireContext(), LinearLayoutManager.VERTICAL, false)
        container_listUser.adapter = adapterUser
        showLoading(true)
        viewModel.getListUser()

        adapterUser.setOnClickListener(object : AdapterUser.OnClickListenerCallback {
            override fun onClick(user: Users, position: Int) {
                this@DataUser.let {
                    if (dialogBannedPengguna.parent != null) {
                        val viewGroup = dialogBannedPengguna.parent as ViewGroup
                        viewGroup.removeView(dialogBannedPengguna)
                    }
                    val dialog = AlertDialog.Builder(requireContext())
                    dialog.setView(dialogBannedPengguna)
                    showDialog = dialog.create()
                    showDialog.show()

                    dialogBannedPengguna.btn_banned_pengguna.setOnClickListener {
                        Toast.makeText(
                            dialogBannedPengguna.context,
                            "Pengguna ini dinonaktifkan",
                            Toast.LENGTH_LONG
                        ).show()
                        showDialog.dismiss()
                    }
                    dialogBannedPengguna.btn_batal_banned.setOnClickListener {
                        showDialog.cancel()
                    }
                }
            }
        })
    }

    private fun observeGetDataUser() = Observer<MutableList<Users>> { listUser ->
        if (listUser.isEmpty()) {
            container_listUser.visibility = View.GONE
            showLoading(false)
        } else {
            adapterUser.setData(listUser)
            container_listUser.visibility = View.VISIBLE
            showLoading(false)
        }

    }

    fun showLoading(state: Boolean) {
        if (state) {
            pb_user.visibility = View.VISIBLE
        } else {
            pb_user.visibility = View.GONE
        }

    }

    private fun onError() = Observer<String> {
        activity?.runOnUiThread {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }

    }
}