package com.example.userrongsok.admin.user

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.userrongsok.R
import com.example.userrongsok.model.Users
import kotlinx.android.synthetic.main.item_list_user.view.*

class AdapterUser(private val data: MutableList<Users> = mutableListOf()) :
    RecyclerView.Adapter<AdapterUser.ViewHolder>() {
    private lateinit var listener: OnClickListenerCallback

    fun setOnClickListener(listener: OnClickListenerCallback){
        this.listener = listener
    }
    inner class ViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
        fun bind(user: Users){
            view.tv_namaPelanggan.text = user.name

            listener.let {
                view.btn_banned.setOnClickListener {
                    listener.onClick(user,layoutPosition)
                }
            }
        }
    }
    fun setData(data: MutableList<Users>){
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    interface OnClickListenerCallback{
        fun onClick(user: Users,position: Int)
    }
}