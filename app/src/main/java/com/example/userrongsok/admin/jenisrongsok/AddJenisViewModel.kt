package com.example.userrongsok.admin.jenisrongsok

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.repo.firebase.FirebaseAdmin
import com.example.userrongsok.util.DispatcherProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class AddJenisViewModel(
    private val repoJenis: FirebaseAdmin,
    private val dispatchers: DispatcherProvider
) : ViewModel() {

    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _successLiveData = MutableLiveData<String>()
    val successData: LiveData<String> get() = _successLiveData

    private val _jenisLiveData = MutableLiveData<List<ModelJenis>>()
    val liveData : LiveData<List<ModelJenis>> get() = _jenisLiveData

    fun addJenis(nama: String) {
        val jenis = ModelJenis(nama)
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoJenis.addJenisRongsok(jenis.nama_jenis)) {
                    is RepositoryResult.Success -> {
                        withContext(dispatchers.main()) {
                            _successLiveData.postValue("Berhasil Menambahkan Jenis Rongsok")
                        }
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        _errorLiveData.value = result.exception.message
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        _errorLiveData.value = result.exception?.message
                    }
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    _errorLiveData.value = e.message
                }
            }
        }
    }

    fun getJenis(){
        CoroutineScope(dispatchers.io()).launch {
            try {
                when(val result = repoJenis.getJenisRongsok()){
                    is RepositoryResult.Success ->{
                        _jenisLiveData.postValue(result.data)
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()){
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> {
                        withContext(dispatchers.main()){
                            result.exception?.message.let {
                                _errorLiveData.postValue(it)
                            }
                        }
                    }
                }
            } catch (e: Exception){
                withContext(dispatchers.main()){
                    e.message?.let { _errorLiveData.value = it }
                }
            }
        }
    }

    class Factory(
        private val repoJenis: FirebaseAdmin,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return AddJenisViewModel(repoJenis, dispatchers) as T
        }
    }
}