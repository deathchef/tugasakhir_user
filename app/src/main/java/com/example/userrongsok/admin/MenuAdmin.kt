package com.example.userrongsok.admin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.userrongsok.R
import kotlinx.android.synthetic.main.layout_main.*
import kotlinx.android.synthetic.main.layout_main.btn_image
import kotlinx.android.synthetic.main.layout_main_admin.*
import kotlinx.android.synthetic.main.main.*
import kotlinx.android.synthetic.main.main_admin.*

class MenuAdmin : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_admin)
        setSupportActionBar(toolbar)

        toolbar_admin.setNavigationIcon(null)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setHomeButtonEnabled(false)
        val navbarConf = findNavController(R.id.nav_host_fragment_admin)
        button_nav_admin.setupWithNavController(navbarConf)

        navbarConf.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.dataUser -> {
                    btn_backAdmin.setImageResource(0)
                    tv_labelAdmin.setText("List Pelanggan")
                    btn_image.setImageResource(0)
                }
                R.id.dataPengepul -> {
                    btn_backAdmin.setImageResource(0)
                    tv_labelAdmin.setText("List Pengepul")
                    btn_image.setImageResource(R.drawable.ic_baseline_history_24)
                }
                R.id.jenisRongsok -> {
                    btn_backAdmin.setImageResource(0)
                    tv_labelAdmin.setText("List Jenis Rongsok")
                    btn_image.setImageResource(0)
                }
            }
        }
    }
}