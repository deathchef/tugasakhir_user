package com.example.userrongsok.admin.jenisrongsok

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.userrongsok.R
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.dialog_tambah_jenis.*
import kotlinx.android.synthetic.main.dialog_tambah_jenis.view.*
import kotlinx.android.synthetic.main.fragment_jenis_rongsok.*
import kotlinx.android.synthetic.main.item_jenis_rongsok.*
import org.koin.android.ext.android.inject

class JenisRongsok : Fragment() {

    private lateinit var viewModel : AddJenisViewModel
    private val factory by inject<AddJenisViewModel.Factory>()
    private val dialog_tambahJenis by lazy {
        layoutInflater.inflate(R.layout.dialog_tambah_jenis,null, false)
    }
    private lateinit var showDialog: AlertDialog
    private lateinit var adapter: AdapterJenisRongsok

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_jenis_rongsok, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = AdapterJenisRongsok()
        viewModel = ViewModelProvider(this, factory).get(AddJenisViewModel::class.java)

        viewModel.errorLiveData.observe(viewLifecycleOwner, onError() )
        viewModel.successData.observe(viewLifecycleOwner, onSuccess())
        viewModel.liveData.observe(viewLifecycleOwner, observeGetDataJenis())

        container_jenisRongsok.layoutManager = LinearLayoutManager(
            this@JenisRongsok.requireContext(), LinearLayoutManager.VERTICAL, false
        )
        container_jenisRongsok.adapter = adapter

        showLoading(true)
        viewModel.getJenis()

        btn_tambahJenis.setOnClickListener {
            this.let {
                if (dialog_tambahJenis.parent != null){
                    val viewGroup = dialog_tambahJenis.parent as ViewGroup
                    viewGroup.removeView(dialog_tambahJenis)
                }
                val dialog = AlertDialog.Builder(requireContext())
                dialog.setView(dialog_tambahJenis)
                showDialog = dialog.create()
                showDialog.show()

                dialog_tambahJenis.btn_add_jenisRongsok.setOnClickListener {
                    if (dialog_tambahJenis.et_nama_jenis?.text.isNullOrEmpty()){
                        dialog_tambahJenis.et_nama_jenis?.error = "Nama Jenis Rongsok Harus Diisi"
                        dialog_tambahJenis.et_nama_jenis?.isFocusable = true
                    }else{
                        observeAddJenis()
                        showDialog.dismiss()
                        viewModel.getJenis()
                    }
                }
                dialog_tambahJenis.btn_cancel_jenisRongsok.setOnClickListener {
                    showDialog.dismiss()
                }
            }
        }
    }

    private fun onSuccess()= Observer<String> {
        activity?.runOnUiThread {
            showDialog.dismiss()
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }

    }

    private fun onError() = Observer<String> {
        activity?.runOnUiThread {
            Toast.makeText(requireContext(),it,Toast.LENGTH_SHORT).show()
        }

    }

    fun observeAddJenis() {
        val namaJenis = dialog_tambahJenis.et_nama_jenis?.text.toString()
        viewModel.addJenis(namaJenis)
    }

    fun observeGetDataJenis()=Observer<List<ModelJenis>>{list_jenis->
        if (list_jenis.isEmpty()){
            container_jenisRongsok.isVisible = false
            showLoading(false)
        }else{
            adapter.setData(list_jenis)
            container_jenisRongsok.isVisible = true
            showLoading(false)
        }
    }

    fun showLoading(state: Boolean){
        if (state){
            pb_jenisRongsok.visibility = View.VISIBLE
        }else{
            pb_jenisRongsok.visibility = View.GONE
        }
    }
}