package com.example.userrongsok.admin.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.userrongsok.model.RepositoryResult
import com.example.userrongsok.model.Users
import com.example.userrongsok.repo.firebase.FirebaseAdmin
import com.example.userrongsok.util.DispatcherProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class UserViewModel(
    private val repoUser: FirebaseAdmin,
    private val dispatchers: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _listUser = MutableLiveData<MutableList<Users>>()
    val listUser: LiveData<MutableList<Users>> get() = _listUser

    fun getListUser() {
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoUser.getDataUser()) {
                    is RepositoryResult.Success -> {
                        _listUser.postValue(result.data)
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    e.message?.let { _errorLiveData.postValue(it) }
                }
            }
        }
    }

    class Factory(
        private val repoUser: FirebaseAdmin,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return UserViewModel(repoUser, dispatchers) as T
        }
    }
}