package com.example.userrongsok.admin.pengepul

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.userrongsok.R
import com.example.userrongsok.model.Pengepul
import com.example.userrongsok.model.Users
import kotlinx.android.synthetic.main.item_list_user.view.*

class AdapterPengepul(private val data: MutableList<Pengepul> = mutableListOf()) :
    RecyclerView.Adapter<AdapterPengepul.ViewHolder>() {
    private lateinit var listener: OnClickListenerCallback

    fun setOnClickListener(listener: OnClickListenerCallback){
        this.listener  = listener
    }

    inner class ViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
        fun bind(pengepul: Pengepul){
            view.tv_namaPelanggan.text = pengepul.name
            listener.let {
                view.btn_banned.setOnClickListener {
                    listener.onClick(pengepul, layoutPosition)
                }
            }
        }
    }
    fun setData(data: MutableList<Pengepul>){
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    interface OnClickListenerCallback{
        fun onClick(pengepul: Pengepul, position: Int)
    }
}