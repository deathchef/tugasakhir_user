package com.example.userrongsok.admin.jenisrongsok

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.userrongsok.R
import kotlinx.android.synthetic.main.item_jenis_rongsok.view.*

class AdapterJenisRongsok(private val data: MutableList<ModelJenis> = mutableListOf()):
    RecyclerView.Adapter<AdapterJenisRongsok.ViewHolder>() {
    inner class ViewHolder(private val view: View):RecyclerView.ViewHolder(view) {
        fun bind(jenis: ModelJenis){
            view.tv_namaJenis.text = jenis.nama_jenis
        }
    }
    fun setData(data: List<ModelJenis>){
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_jenis_rongsok, null, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }
}