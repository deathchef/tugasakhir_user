package com.example.userrongsok.model

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize
import java.util.ArrayList

@Parcelize
data class Transaksi(
    var catatan: String="",
    var foto_transaksi: String="",
    var id_pengepul:String ="",
    var id_user: String ="",
    var isDone: Boolean = false,
    var isProccess: Boolean =false,
    var isCancel: Boolean = false,
    var isRated: Boolean = false,
    var status: String ="",
    var jumlah_harga: Int = 0,
    var nama_pelanggan: String = "",
    var nama_toko: String = "",
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var address: String = "",
    var noTelpToko: String = "",
    var tanggal_transaksi: Timestamp= Timestamp.now()
): Parcelable