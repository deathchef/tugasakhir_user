package com.example.userrongsok.model

import android.net.Uri
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Rongsok(
    var foto_rongsok: String = "",
    var harga_rongsok: Int = 0,
    var jenis_rongsok: String = "",
    var nama_rongsok: String = "",
    var id_toko: String = "",
    var isReady: Boolean =false
) : Parcelable

@Parcelize
data class RongsokAdapter(
    var foto_rongsok: String,
    var foto_rongsokUri: Uri,
    var harga_rongsok: Int = 0,
    var jenis_rongsok: String = "",
    var nama_rongsok: String = "",
    var id_toko: String = "",
    var isReady: Boolean =false,
    var quantity: Int = 0
) : Parcelable

data class RongsokTransaksi(
    var foto_rongsok: String="",
    var nama_rongsok: String ="",
    var id_user: String = "",
    var id_toko: String = "",
    var harga_rongsok: Int = 0,
    var quantity: Int = 0
)

data class RongsokTransaksiAdapter(
    var foto_rongsokUri: Uri,
    var foto_rongsok: String,
    var nama_rongsok: String ="",
    var id_user: String ="",
    var id_toko: String = "",
    var harga_rongsok: Int = 0,
    var quantity: Int =0
)
//
//@Parcelize
//data class ListRongsokAdapter(
//    val list_rongsok: RongsokAdapter,
//    var position : Int
//) : Parcelable