package com.example.userrongsok.model

import android.os.Parcelable
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FieldValue
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BodyPayloadNotif(
    val data: Data,
    val date: Timestamp,
    val to: String
) : Parcelable {
    @Parcelize
    data class Data(
        val id_pengepul: String? = null,
        val id_userRongsok: String? = null,
        val nama_user: String? = null,
        val message: String? = null,
        val title: String? = null,
        val type: String? = null
    ): Parcelable
}
