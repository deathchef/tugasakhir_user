package com.example.userrongsok.model

import android.net.Uri
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Toko(
    var foto_toko: String ="",
    var nama_toko: String = "",
    var rating: Double = 0.0,
    var pengunjung: Double = 0.0,
    var id_pengepul: String = "",
    var latitude: Double = 0.0,
    var longitude: Double =0.0,
    var address: String ="",
    var tokenFcm: String = "",
    var noTelp: String = "",
    var isOpen: Boolean = false
): Parcelable

@Parcelize
data class TokoAdapter(
    var foto_toko: String,
    var foto_tokoUri: Uri,
    var nama_toko: String = "",
    var rating: Double = 0.0,
    var pengunjung: Double = 0.0,
    var id_pengepul: String = "",
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var address: String ="",
    var tokenFcm: String = "",
    var noTelp: String = "",
    var isOpen: Boolean = false
) :Parcelable