package com.example.userrongsok.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Notifications(
    var data: Map<String, String> = mutableMapOf(),
    var date: com.google.firebase.Timestamp? = null,
    var to: String = ""
): Parcelable