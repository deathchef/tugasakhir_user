package com.example.userrongsok.model

import com.google.android.gms.tasks.Task
import com.google.firebase.installations.InstallationTokenResult

data class Users(
    var id: String = "",
    var name: String = "",
    var email: String = "",
    var phone: String = "",
    var banned: Boolean = false,
    var tokenNotif: String = ""
)

data class UserDB(
    var id: String = "",
    var name: String = "",
    var tokenFcm: String = ""
)