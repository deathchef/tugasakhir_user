package com.example.userrongsok.model

import java.lang.Exception

sealed class RepositoryResult<out R> {
    data class Success<out T>(val data: T): RepositoryResult<T>()
    data class Error(val exception: Exception): RepositoryResult<Nothing>()
    data class Canceled(val exception: Exception?): RepositoryResult<Nothing>()

    override fun toString(): String {
        return when(this){
            is Success<*> -> "Success [data = $data]"
            is Error -> "Error[exception=$exception]"
            is Canceled ->"Canceled{exceptio = $exception]"
        }
    }
}