package com.example.userrongsok.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Pengepul(
    var id: String = "",
    var name: String = "",
    var email: String = "",
    var password: String = "",
    var noHp: String = "",
    var tokenFcm : String="",
    var isBanned: Boolean = false
): Parcelable