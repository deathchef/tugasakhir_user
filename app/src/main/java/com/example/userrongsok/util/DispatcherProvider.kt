package com.example.userrongsok.util

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

interface DispatcherProvider {
    fun main(): CoroutineDispatcher = Dispatchers.Main
    fun default(): CoroutineDispatcher = Dispatchers.Default
    fun io(): CoroutineDispatcher = Dispatchers.IO
    fun undefined(): CoroutineDispatcher = Dispatchers.Unconfined
}

class DefaultDispatcherProvider: DispatcherProvider