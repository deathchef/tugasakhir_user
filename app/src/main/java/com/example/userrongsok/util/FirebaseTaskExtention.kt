package com.example.userrongsok.util

import com.example.userrongsok.model.RepositoryResult
import com.google.android.gms.tasks.Task
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

object FirebaseTaskExtention {
    suspend fun <T> Task<T>.awaits(): RepositoryResult<T> {
        if (isComplete) {
            val e = exception
            return if (e == null) {
                if (isCanceled) {
                    RepositoryResult.Canceled(CancellationException("Task $this telah ditunda secara otomatis"))
                } else {
                    @Suppress("UNCHECKED_CAST")
                    RepositoryResult.Success(result as T)
                }
            } else {
                RepositoryResult.Error(e)
            }
        }
        return suspendCancellableCoroutine { cont->
            addOnCompleteListener {
                val e = exception
                if (e == null){
                    @Suppress("UNCHECKED_CAST")
                    if (isCanceled) cont.cancel() else cont.resume(RepositoryResult.Success(result as T))
                }else{
                    cont.resumeWithException(e)
                }
            }
        }
    }
}