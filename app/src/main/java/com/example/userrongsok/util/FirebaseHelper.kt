package com.example.userrongsok.util

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

val firestore = Firebase.firestore

class FirebaseHelper(private val auth: FirebaseAuth) {
    fun firebaseLogin():Boolean{
        return auth.currentUser != null
    }

    fun firebaseSignOut(){
        auth.signOut()
    }

    fun getUsername(): String{
        return auth.currentUser?.displayName!!
    }
}