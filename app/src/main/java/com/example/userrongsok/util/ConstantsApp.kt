package com.example.userrongsok.util

object ConstantsApp {

    const val USER = "users"
    const val PENGEPUL = "pengepul"
    const val JENIS_RONGSOK = "jenis_rongsok"
    const val TOKO = "toko"
    const val RONGSOK = "rongsok"
    const val RONGSOK_TRANSAKSI = "rongsok_transaksi"
    const val LIST_TRANSAKSI = "list_transaksi"
    const val TRANSAKSI = "transaksi"
    const val SERVER_KEY: String =
        "key=AAAA46B_VmQ:APA91bFQkDwzsgDvZxL0201-zrpMmbAs0hjHEDIrypoXhiRHHE0MmAfPRu60qYXtJHuwfGE4Y3bAI8_kwuhSOx40_J3terfVrKqafs3JnNOS924wicDu2_5auR9bAceMZGhuCWwiZAME"
    const val CONTENT_TYPE = "application/json;charset=UTF-8"
    const val BASE_URL_NOTIF = "https://fcm.googleapis.com/"
    const val SEND_NOTIF = "fcm/send"
    const val NOTIFICATION = "notifications"
    const val SHOP = "Shop"
    const val CHAT = "Chat"
    const val TITLE = "title"
    const val MESSAGE = "message"
    const val TYPE = "type"
}