package com.example.userrongsok.util

import com.example.userrongsok.admin.jenisrongsok.AddJenisViewModel
import com.example.userrongsok.admin.pengepul.PengepulViewModel
import com.example.userrongsok.admin.user.UserViewModel
import com.example.userrongsok.repo.firebase.*
import com.example.userrongsok.ui.login.LoginViewModel
import com.example.userrongsok.ui.mainmenu.detailpengepul.DetailViewModel
import com.example.userrongsok.ui.mainmenu.detailtransaksi.DetailTransaksiViewModel
import com.example.userrongsok.ui.mainmenu.historytransaksi.HistoryViewModel
import com.example.userrongsok.ui.mainmenu.pengepul.DataTransaksiViewModel
import com.example.userrongsok.ui.mainmenu.pengepul.listpengepul.LIstPengepulViewModel
import com.example.userrongsok.ui.mainmenu.profil.ProfilViewModel
import com.example.userrongsok.ui.mainmenu.transaksi.TransaksiViewModel
import com.example.userrongsok.ui.mainmenu.transaksi.detailtransaksinot.DetailTransaksiNotVM
import com.example.userrongsok.ui.notification.NotificationViewModel
import com.example.userrongsok.ui.regis.RegisterViewModel
import org.koin.dsl.module

var koinModule = module {
    factory { FirebaseHelper(get()) }
}

val firebaseRepo = module {
    factory { FirebaseUserRepo(get()) }
    factory { FirebaseAuthRepo(get()) }
    factory { FirebaseAdmin() }
    factory { FirebasePengepulRepo() }
    factory { FirebaseStorageRepo(get()) }
    factory { FirebaseTransaksiRepo(get()) }
    factory { FirebaseNotificationRepo() }
}

val viewModel = module {
    factory { LoginViewModel.Factory(get(),get(),get(),get()) }
    factory { RegisterViewModel.Factory(get(), get(),get()) }
    factory { PengepulViewModel.Factory(get(), get()) }
    factory { UserViewModel.Factory(get(), get()) }
    factory { AddJenisViewModel.Factory(get(), get()) }
    factory { ProfilViewModel.Factory(get(), get()) }
    factory { LIstPengepulViewModel.Factory(get(),get(),get()) }
    factory { DetailViewModel.Factory(get(), get(), get()) }
    factory { DetailTransaksiViewModel.Factory(get(),get(),get(),get())}
    factory { TransaksiViewModel.Factory(get(),get(),get())}
    factory { DetailTransaksiNotVM.Factory(get(),get(),get()) }
    factory { DataTransaksiViewModel.Factory(get(),get(),get(),get()) }
    factory { HistoryViewModel.Factory(get(),get(),get()) }
    factory { NotificationViewModel.Factory(get(),get(),get()) }
}