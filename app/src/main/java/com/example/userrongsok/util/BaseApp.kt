package com.example.userrongsok.util

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class BaseApp: Application() {
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this@BaseApp)
    }
    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        startKoin {
            androidContext(this@BaseApp)
            modules(module {
                single { FirebaseAuth.getInstance() }
                single { Firebase.storage.reference }
                single { DefaultDispatcherProvider() as DispatcherProvider }
            })
            modules(koinModule)
            modules(viewModel)
            modules(firebaseRepo)
        }
    }
}